#include "gamesettings.h"

GameSettings::GameSettings(QObject *parent) : QObject(parent)
{
    theSettings = new QSettings("LowRentGames", "Scribble" );

    if ( !theSettings->contains("HostAddress") )
    {
        theSettings->setValue("HostAddress", "18.191.209.139:443");
    }
}

GameSettings::~GameSettings()
{
    delete theSettings;
}

bool GameSettings::ValueExists(QString Key)
{
    return theSettings->contains( Key );
}

QVariant GameSettings::GetValue(QString Key)
{
    return theSettings->value( Key );
}

void GameSettings::SetValue(QString Key, QVariant Value)
{
    theSettings->setValue( Key, Value );
}

void GameSettings::ClearValue(QString Key)
{
    if ( theSettings->contains(Key) )
    {
        theSettings->remove( Key );
    }
}
