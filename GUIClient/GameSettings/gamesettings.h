#ifndef GAMESETTINGS_H
#define GAMESETTINGS_H

#include <QObject>
#include <QSettings>
#include <QVariant>

class GameSettings : public QObject
{
    Q_OBJECT
public:
    explicit GameSettings(QObject *parent = nullptr);
    ~GameSettings();

    bool ValueExists( QString Key );
    QVariant GetValue( QString Key );
    void SetValue( QString Key, QVariant Value );
    void ClearValue( QString Key );

signals:

public slots:

private:
    QSettings * theSettings;
};

#endif // GAMESETTINGS_H
