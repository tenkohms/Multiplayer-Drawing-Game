#include "GameManager/gamemanager.h"
#include <QDebug>

#include "SocketObject/gameclientsocket.h"
#include "MessageProcessor/messageprocessor.h"
#include "MessageProcessor/clientmessageprocessor.h"

#include <QFile>
#include <QStandardPaths>
#include <QSettings>

GameManager::GameManager(QObject *parent) :
    QObject(parent),
    socketObject_( Q_NULLPTR ),
    messageProcessor( Q_NULLPTR ),
    artist_( QString() ),
    currentRound_( 0 ),
    flag_( 0 ),
    roomCode_( 0 )
{
    gameSettings = new GameSettings( this );

    connect( &clientController, SIGNAL(setNewPage(QString)), this, SLOT(SetNewLoaderPage(QString)));
    connect( &clientController, SIGNAL(newRoundWord(QString)), this, SLOT(ReceiveRoundWord(QString)));
    connect( &clientController, SIGNAL(newRoundGuess(QString)), this, SLOT(ReceiveRoundImage( QString )));
    connect( &clientController, SIGNAL(artistName(QString)), this, SLOT( TrackArtist(QString)));
    connect( &clientController, SIGNAL(currentRound(int)), this, SLOT(TrackRound(int)));
    connect( &clientController, SIGNAL(fgIsImage(bool)), this, SLOT(fgIsImageSlot(bool)));
    connect( &clientController, SIGNAL(fgGuess(QString)), this, SLOT(fgGuessSlot(QString)));
    connect( &clientController, SIGNAL(fgRounds(int)), this, SLOT(fgRoundsSlot(int)));
    connect( &clientController, SIGNAL(fgCurrentPlayer(QString)), this, SLOT(fgCurrentPlayerSlot(QString)));
    connect( &clientController, SIGNAL(fgDisplayWord(QString)), this, SLOT( fgDisplayWordSlot(QString)));
}

GameManager::~GameManager()
{
    //cleanup if our pointers are not null
    if ( socketObject_ != Q_NULLPTR)
        socketObject_->deleteLater();
    if ( messageProcessor != Q_NULLPTR)
        messageProcessor->deleteLater();
}

void GameManager::ConnectToHost()
{
    QString hostAddress = gameSettings->GetValue( "HostAddress" ).toString();
    //We are the game host, make host type socketobject and appropiate connections
    socketObject_ = new GameClientSocket(this);

    //connections
    connect( socketObject_, SIGNAL(connected()), this, SLOT(SocketObjectConnected()));
    connect( socketObject_, SIGNAL(disconnected()), this, SLOT(SocketObjectDisconnected()));
    connect( socketObject_, SIGNAL(readyForUsername()), this, SLOT(ReadyForUsername()));
    connect( socketObject_, SIGNAL(readyToHostOrJoin()), this, SLOT(ReadyToHostOrJoin()));

    //attempt to create connection to HOST
    socketObject_->BeginConnection(hostAddress);
}

void GameManager::SocketObjectConnected()
{
    messageProcessor = new ClientMessageProcessor( this );

    //socketObject <--> messageProcessor connections
    connect( socketObject_, SIGNAL(newMessageReceived(QString)), messageProcessor, SLOT(ProcessMessage(QString)));
    connect( messageProcessor, SIGNAL(registerAlias(QString,QString)), socketObject_, SLOT(RegisterAlias(QString,QString)));
    connect( messageProcessor, SIGNAL(aliasSetFailed(bool)), this, SLOT(AliasSetFailed(bool)));
    connect( messageProcessor, SIGNAL( ipBan()), this, SLOT(IPBan()));
    //messageProcessor <--> this connections
    connect( messageProcessor, SIGNAL(clientList(int, QStringList)), this, SLOT(ReceiveClientList(int, QStringList)));
    connect( messageProcessor, SIGNAL(chatMessage(QString,QString)), this, SLOT(ChatMessageSlot(QString,QString)));
    connect( messageProcessor, SIGNAL(guessesReceivedProgress(int,int, int, int)), this, SLOT(GuessesReceivedProgressSlot(int,int, int, int)));
    connect( messageProcessor, SIGNAL(roundReady(QString)), this, SLOT( RoundReady(QString)));
    connect( messageProcessor, SIGNAL( newGuessRound(int,QString,bool,QString)), this, SLOT( ReceiveNewGuessRound(int,QString,bool,QString)));
    connect( messageProcessor, SIGNAL( finalGuesses(int,QString,QString,QMap<int,bool>,QMap<int,QPair<QString,QString> >,QStringList)), this, SLOT( ReceiveFinalGuesses(int,QString,QString,QMap<int,bool>,QMap<int,QPair<QString,QString> >,QStringList)));
    connect( messageProcessor, SIGNAL( changeRound(int)), this, SLOT(fgChangeRoundSlot(int)));
    connect( messageProcessor, SIGNAL(voteSelect(QString)), this, SLOT(DisplayVotedFor(QString)));
    connect( messageProcessor, SIGNAL(finalScores(QMap<QString,int>)), this, SLOT(ReceiveScores(QMap<QString,int>)));
    connect( messageProcessor, SIGNAL(joinError(QString)), this, SLOT(JoinError(QString)));
    connect( messageProcessor, SIGNAL(ReadyList(QStringList)), this, SLOT(ReadyList(QStringList)));
}

void GameManager::SocketObjectDisconnected() {
    //notify UI we have lost connection
    emit enableChat( false );
    emit newNotification( "Disconnected", "red" );

    //clean up objects
    if ( messageProcessor != Q_NULLPTR )
    {
        MessageProcessor * oldMP = messageProcessor;
        oldMP->deleteLater();
    }
    if( socketObject_ != Q_NULLPTR )
    {
        SocketObjectBase * oldSocket = socketObject_;
        oldSocket->deleteLater();
    }

    //Set as QNULL Ptr so we know in destructor to leave this alone
    messageProcessor = Q_NULLPTR;
    socketObject_ = Q_NULLPTR;

//    //force UI back to home screen
    emit setNewMainLoaderPage( "mainMenuPage.qml");
}

void GameManager::ReadyForUsername() {
    if ( gameSettings->ValueExists( "Username" ) )
    {
        QString username = gameSettings->GetValue( "Username" ).toString().replace("\\","").replace("\"","");
        RegisterAliasSlot(username);
    }
    else
    {
        emit setNewMainLoaderPage("setAliasPage.qml");
    }
}

void GameManager::ReadyToStart()
{
    QString message = "{\"type\":\"readyToStart\", \"roomCode\":" + QString::number( roomCode_) + ",\"sender\":\"" + socketObject_->WhoAmI() + "\"}";
    socketObject_->SendSocketMessage( message );
}

void GameManager::RequestClientList()
{
    socketObject_->SendSocketMessage("{\"type\":\"reqClientList\",\"sender\":\"" + socketObject_->WhoAmI() + "\"}");
}

void GameManager::ChatMessageSlot(QString message, QString clientID)
{
    //let UI know socket received a new chat message
    emit newChatMessage( message, clientID );
}

void GameManager::ChatMessageToSocketSlot(QString message)
{
    //function used to forward chat message from UI for sending via socket

    message.replace("\"","").replace("\\","");
    //Off goes the message to socket land
    if ( socketObject_->isConnected() )
    {
        socketObject_->BroadcastChatMessage( message );
    }
}

void GameManager::ReadyList(QStringList rList)
{
    emit readyList( rList );
}

void GameManager::ReSendQMLSlot()
{
    socketObject_->ResendMessage();
}

void GameManager::AskForClients()
{
    //update lobby ui with cached clients
    emit newClientList( cachedClientList_);
}

void GameManager::RoundReady(QString Word)
{
    artist_ = socketObject_->WhoAmI();
    currentRound_ = 0;
    clientController.StartRound( Word );
}

void GameManager::ReceiveRoundWord(QString Word)
{
    emit newRoundWord( Word );
}

void GameManager::ReceiveRoundImage(QString imageFileLocation)
{
    emit newGuessWord( imageFileLocation );
}

void GameManager::receiveImageFileData( QString fileData )
{
    qDebug() << "Receiving image:";
    QString message = messageProcessor->CreateImageFileMessage(roomCode_, currentRound_, fileData, artist_, socketObject_->WhoAmI() );
    socketObject_->SendSocketMessage( message );
}

void GameManager::ReceiveGuessFromGuessPage(QString Guess)
{
    QString message = messageProcessor->CreateGuess(roomCode_, currentRound_, Guess, artist_, socketObject_->WhoAmI() );
    socketObject_->SendSocketMessage( message );
}

void GameManager::ReceiveFinalGuesses(int rounds, QString op, QString word, QMap<int, bool> roundIsImageMap, QMap<int, QPair<QString, QString> > roundPlayerGuess, QStringList displayWord)
{
    emit setNewMainLoaderPage( "VotePage.qml");
    if ( op == socketObject_->WhoAmI() )
    {
        emit isOP();
    }
    else
    {
        emit isNotOP();
    }
    clientController.ReceiveFinalGuesses(rounds, op, word, roundIsImageMap, roundPlayerGuess, displayWord );
}

void GameManager::GuessPageRoundRequest(int round)
{
    clientController.FinalGuessRoundRequest( round );
}

void GameManager::GuessPageVoteSelect(QString player)
{
    QString message=  messageProcessor->CreateVoteSelectedMessage(roomCode_, player );
    socketObject_->SendSocketMessage( message );
}

void GameManager::DisplayVotedFor(QString player)
{
    QString message = player + " selected! ";
    emit newNotification( message, "yellow" );
    emit setNewMainLoaderPage( "LoadingPage.qml");
}

void GameManager::fgIsImageSlot(bool isImage )
{
    emit fgIsImage( isImage );
}

void GameManager::fgGuessSlot( QString Guess )
{
    emit fgGuess( Guess );
}
void GameManager::fgWordSlot(QString Word )
{
    emit fgWord( Word);
}

void GameManager::fgRoundsSlot(int rounds)
{
    emit fgRounds( rounds );
}

void GameManager::fgCurrentPlayerSlot(QString Player)
{
    emit fgCurrentPlayer( Player );
}

void GameManager::fgChangeRoundSlot( int Round )
{
    emit fgChangeRound( Round );
}

void GameManager::fgQMLRoundChange(int round )
{
    QString message = messageProcessor->CreateChangeRoundMessage( roomCode_, round ) ;
    socketObject_->SendSocketMessage( message );
}

void GameManager::fgDisplayWordSlot(QString word)
{
    emit fgDisplayWord( word );
}

void GameManager::ReceiveScores(QMap<QString, int> scores)
{
    QString scoreDisplay;
    foreach (const QString player, scores.keys() )
    {
        scoreDisplay.append( player + " : " + QString::number( scores[player]  ) + "\n");
    }

    emit score( scoreDisplay );
    emit setNewMainLoaderPage( "ScoreScreen.qml");
}

void GameManager::TrackArtist(QString artist)
{
    artist_ = artist;
}

void GameManager::TrackRound(int round)
{
    currentRound_ = round;
}

void GameManager::ReceiveNewGuessRound(int round, QString artist, bool isImage, QString Data)
{
    clientController.ReceiveGuessClient(round, artist, isImage, Data);
}

void GameManager::RegisterAliasSlot(QString Alias)
{
    gameSettings->SetValue( "Username", Alias );
    socketObject_->RegisterAlias( Alias );
}

void GameManager::AliasSetFailed(bool failed )
{
    if ( failed )
    {
        emit aliasSetFailed( failed );
    }
}

void GameManager::GuessesReceivedProgressSlot(int progress, int total, int round, int rTotal)
{
    emit guessesReceivedProgress( progress );
    emit guessesTotalProgress( total );
    emit roundProgress( round );
    emit roundTotal( rTotal );
}

QString GameManager::GameSettingsUsername()
{
    if ( gameSettings->ValueExists("Username") )
    {
        return gameSettings->GetValue("Username").toString();
    }
    else
    {
        return "Not Set";
    }
}

void GameManager::ClearGameSettingsUsername() {
    if ( gameSettings->ValueExists("Username") )
    {
        gameSettings->ClearValue( "Username" );
    }
}

QString GameManager::GameSettingsServerAddress()
{
    if ( gameSettings->ValueExists("HostAddress") )
    {
        return gameSettings->GetValue("HostAddress").toString();
    }
    else
    {
        return "Not Set";
    }
}

void GameManager::SetGameSettingsServerAddress(QString NewAddress)
{
    QVariant val = NewAddress;
    gameSettings->SetValue("HostAddress", val);
}

void GameManager::ReadyToHostOrJoin()
{
    emit setNewMainLoaderPage("HostOrJoin.qml");
}

QString GameManager::RoomCode()
{
    return QString::number( roomCode_ );
}

void GameManager::HostNewGame()
{
    QString message = "{\"type\":\"createRoom\",\"clientID\":\"" + socketObject_->WhoAmI() + "\"}";
    socketObject_->SendSocketMessage( message );
}

void GameManager::JoinUsingRoomCode(QString roomCode)
{
    QString message = "{\"type\":\"joinRoom\",\"clientID\":\"" + socketObject_->WhoAmI() + "\",\"roomCode\":" + roomCode + "}";
    socketObject_->SendSocketMessage( message );
}

void GameManager::JoinError(QString error)
{
    emit joinError( error );
}

void GameManager::ReceiveClientList(int roomCode, QStringList Clients)
{
    mMutex.lock();
    if ( roomCode_ != roomCode )
    {
        roomCode_ = roomCode;
        emit setNewMainLoaderPage( "lobbyPage.qml");
    }
    cachedClientList_ = Clients;
    emit newClientList( Clients );
    emit enableChat( true );
    mMutex.unlock();
}

void GameManager::SetNewLoaderPage(QString Page)
{
    emit setNewMainLoaderPage( Page );
}

void GameManager::IPBan()
{
    emit banError();
}
