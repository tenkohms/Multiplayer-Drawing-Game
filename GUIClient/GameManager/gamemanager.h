#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H

#include <QObject>
#include <QMap>
#include <QPair>
#include <QMutex>

#include "SocketObject/socketobjectbase.h"
#include "MessageProcessor/messageprocessor.h"
#include "GameClientController/gameclientcontroller.h"
#include "GameSettings/gamesettings.h"

class GameManager : public QObject
{
    Q_OBJECT

public:
    explicit GameManager(QObject *parent = nullptr);
    ~GameManager();


signals:
    void setNewMainLoaderPage( QString Page );
    void newNotification( QString message, QString color);
    void isOP();
    void isNotOP();
    void score( QString scores );

    void aliasSetFailed( bool );
    void newClientList( QStringList );
    void newChatMessage( QString message, QString clientID );
    void readyList( QStringList rList );

    void newRoundWord( QString word );
    void newGuessWord( QString imageFileLocation );

    void fgIsImage( bool );
    void fgGuess( QString );
    void fgWord( QString );
    void fgRounds( int );
    void fgCurrentPlayer( QString );
    void fgChangeRound( int );
    void fgDisplayWord( QString);

    void guessesReceivedProgress( int progress );
    void guessesTotalProgress( int total );
    void roundProgress( int round );
    void roundTotal( int rTotal );

    void enableChat( bool enable );

    void joinError( QString error );
    void banError();

public slots:
    void SetNewLoaderPage( QString Page );
    void ReceiveClientList(int roomCode, QStringList Clients );
    void AskForClients();
    void ChatMessageSlot( QString message, QString clientID );
    void ChatMessageToSocketSlot( QString message );
    void ReadyList( QStringList rList );
    void ReSendQMLSlot();


    void ConnectToHost();
    void ReadyForUsername();
    void HostNewGame();
    void JoinUsingRoomCode( QString roomCode );
    void JoinError( QString error );

    void SocketObjectConnected();
    void SocketObjectDisconnected();

    void ReadyToStart();
    void RequestClientList();

    void RoundReady( QString Word );
    void ReceiveRoundWord( QString Word );
    void ReceiveRoundImage( QString imageFileLocation );
    void receiveImageFileData( QString );
    void ReceiveNewGuessRound( int round, QString artist, bool isImage, QString Data );
    void ReceiveGuessFromGuessPage( QString Guess );
    void ReceiveFinalGuesses( int rounds, QString op, QString word, QMap< int, bool> roundIsImageMap, QMap< int, QPair< QString, QString> > roundPlayerGuess, QStringList displayWord );
    void GuessPageRoundRequest( int round );
    void GuessPageVoteSelect( QString player );
    void DisplayVotedFor( QString player );

    void fgIsImageSlot( bool );
    void fgGuessSlot( QString );
    void fgWordSlot( QString );
    void fgRoundsSlot( int );
    void fgCurrentPlayerSlot( QString );
    void fgChangeRoundSlot( int );
    void fgQMLRoundChange( int );
    void fgDisplayWordSlot( QString );
    void ReceiveScores( QMap< QString, int> );

    void TrackArtist( QString artist );
    void TrackRound( int round );

    void RegisterAliasSlot( QString Alias );
    void AliasSetFailed( bool );
    void GuessesReceivedProgressSlot( int, int, int, int );

    QString GameSettingsUsername();
    void ClearGameSettingsUsername();
    QString GameSettingsServerAddress();
    void SetGameSettingsServerAddress( QString NewAddress );
    void ReadyToHostOrJoin();
    QString RoomCode();

    void IPBan();

private:
    GameSettings * gameSettings;

    SocketObjectBase * socketObject_;
    MessageProcessor * messageProcessor;
    QStringList cachedClientList_;
    GameClientController clientController;

    QString artist_;
    int currentRound_;

    int flag_;
    int roomCode_;
    QMutex mMutex;

};


#endif // GAMEMANAGER_H
