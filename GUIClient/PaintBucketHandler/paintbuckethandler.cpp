#include "paintbuckethandler.h"
#include <QImage>

#include<QDebug>
#include<QPair>
#include<QList>
#include<QFile>
#include <QStandardPaths>
#include <QTime>


PaintBucketHandler::PaintBucketHandler(QObject *parent) : QObject(parent)
{
    qDebug() << filePath();
}

void PaintBucketHandler::UsePaintBucket(int CanvasWidth, int CanvasHeight, int XPos, int YPos, QString ImageFile, int NewR, int NewG, int NewB, bool isRainbow )
{
    QTime time = QTime::currentTime();
    qsrand( (uint)time.msec() );

    QImage imageFile;
    if ( !imageFile.load( ImageFile ) )
    {
        return;
    }

    if ( imageFile.width() > CanvasWidth )
    {
        XPos = XPos * ( imageFile.width() / CanvasWidth );
    }

    if ( imageFile.height() > CanvasHeight )
    {
        YPos = YPos * ( imageFile.height() / CanvasHeight );
    }

    imageFile = imageFile.convertToFormat(QImage::Format_RGBA8888);

    uchar *bits = imageFile.bits();

    int origR, origG, origB;
    int begPixel = XPos * 4 + YPos * 4 * imageFile.width();

    origR = bits[ begPixel ];
    origG = bits[ begPixel + 1];
    origB = bits[ begPixel + 2 ];

    if ( ( !isRainbow ) &&  ( ( origR == NewR ) && ( origG == NewG ) && ( origB == NewB ) ) )
    {
        emit finishedBucket( QString("file:///" + ImageFile ) );
        return;
    }



    bool reachLeft( false );
    bool reachRight( false );

    int rainbowTicker = 0;
    int rainbowSel = 0;
    int swap = 0;

    QColor newColor = NewColor( rainbowSel );
    if ( isRainbow )
    {
        NewR = newColor.red();
        NewG = newColor.green();
        NewB = newColor.blue();
    }

    QList< QPair<int, int> > pixelStack;
    pixelStack.push_back( QPair< int, int>( XPos, YPos ) );
    while( pixelStack.length() > 0 )
    {

        if ( isRainbow )
        {
            rainbowTicker++;
            swap = qrand() % ( 2 );
        }
        if ( rainbowTicker >= 5 && swap )
        {
            rainbowTicker = 0;
            rainbowSel++;

            if (rainbowSel > 6)
                rainbowSel = 0;

            newColor = NewColor( rainbowSel );
            NewR = newColor.red();
            NewG = newColor.green();
            NewB = newColor.blue();
        }
        reachLeft = false;
        reachRight = false;

        QPair<int, int> currentPixel = pixelStack[0];
        pixelStack.pop_front();
        begPixel = currentPixel.first * 4 + currentPixel.second * 4 * imageFile.width();

        while( currentPixel.second - 1 >= 0 )
        {
            begPixel = currentPixel.first * 4 + currentPixel.second * 4 * imageFile.width();
            if( !( ( bits[ begPixel ] == origR ) && ( bits[ begPixel + 1 ] == origG ) && ( bits[ begPixel + 2] == origB) ) )
            {
                break;
            }
            currentPixel.second--;
        }

        //currentPixel.second++;
        begPixel = currentPixel.first * 4 + currentPixel.second * 4 * imageFile.width();
        bits[ begPixel ] = NewR;
        bits[ begPixel + 1 ] = NewG;
        bits[ begPixel + 2 ] = NewB;
        bits[ begPixel + 3 ] = 255;

        while( currentPixel.second + 1 < imageFile.height() )
        {
            currentPixel.second++;
            begPixel = currentPixel.first * 4 + currentPixel.second * 4 * imageFile.width();
            if( ( ( bits[ begPixel ] == origR ) && ( bits[ begPixel + 1 ] == origG ) && ( bits[ begPixel + 2] == origB) ) )
            {
                if ( isRainbow )
                {
                    rainbowTicker++;
                    swap = qrand() % ( 2 );
                }
                if ( rainbowTicker >= 5 && swap )
                {
                    rainbowTicker = 0;
                    rainbowSel++;

                    if (rainbowSel > 6)
                        rainbowSel = 0;

                    newColor = NewColor( rainbowSel );
                    NewR = newColor.red();
                    NewG = newColor.green();
                    NewB = newColor.blue();
                }
                bits[ begPixel ] = NewR;
                bits[ begPixel + 1 ] = NewG;
                bits[ begPixel + 2 ] = NewB;
                bits[ begPixel + 3 ] = 255;

                if( currentPixel.first > 1 )
                {
                    int leftPixel = ( currentPixel.first - 1 ) * 4 + currentPixel.second * 4 * imageFile.width();
                    if( ( ( bits[ leftPixel ] == origR ) && ( bits[ leftPixel + 1 ] == origG ) && ( bits[ leftPixel + 2] == origB) ) )
                    {
                        if( !reachLeft )
                        {
                            pixelStack.push_back(QPair< int, int> ( currentPixel.first - 1, currentPixel.second ) );
                            reachLeft = true;
                        }
                    }
                    else
                    {
                        reachLeft = false;
                    }
                }

                if ( currentPixel.first < imageFile.width() )
                {
                    int rightPixel = ( currentPixel.first + 1 ) * 4 + currentPixel.second * 4 * imageFile.width();
                    if( ( ( bits[ rightPixel ] == origR ) && ( bits[ rightPixel + 1 ] == origG ) && ( bits[ rightPixel + 2] == origB) ) )
                    {
                        if( !reachRight )
                        {
                            pixelStack.push_back(QPair< int, int> ( currentPixel.first + 1, currentPixel.second ) );
                            reachRight = true;
                        }
                    }
                    else
                    {
                        reachRight = false;
                    }
                }
            }
            else
            {
                if ( isRainbow )
                {
                    rainbowTicker++;
                    swap = qrand() % ( 2 );
                }
                if ( rainbowTicker >= 5 && swap )
                {
                    rainbowTicker = 0;
                    rainbowSel++;

                    if (rainbowSel > 6)
                        rainbowSel = 0;

                    newColor = NewColor( rainbowSel );
                    NewR = newColor.red();
                    NewG = newColor.green();
                    NewB = newColor.blue();
                }
                bits[ begPixel ] = NewR;
                bits[ begPixel + 1 ] = NewG;
                bits[ begPixel + 2 ] = NewB;
                bits[ begPixel + 3 ] = 255;
                break;
            }
        }
    }
    QImage newImage( bits, imageFile.width(), imageFile.height(), QImage::Format_RGBA8888 );
    int addage = 1;
    while ( QFile( QString( QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory) ) + "/imageFile" + QString::number( addage) + ".png" ).exists() )
    {
        addage++;
    }
    newImage.save(QString( QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory)  + "/imageFile" + QString::number( addage) + ".png" ), "PNG");
    emit finishedBucket("file:///" + QString( QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory) ) + "/imageFile" + QString::number( addage) + ".png");
}

QString PaintBucketHandler::filePath()
{
    return QString( QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory) ) + "imageFile.png";
}

void PaintBucketHandler::RemoveFiles()
{
    if (QFile( QString( QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory) ) + "imageFile.png" ).exists() )
    {
        QFile( QString( QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory) ) + "imageFile.png").remove();
    }
    int addage = 1;
    while ( QFile( QString( QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory) ) + "/imageFile" + QString::number( addage) + ".png" ).exists() )
    {
        QFile(QString( QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory) ) + "/imageFile" + QString::number( addage) + ".png" ).remove();
        addage++;
    }
}

QColor PaintBucketHandler::NewColor(int sel)
{
    switch( sel )
    {
    case 0:
        return QColor("red");
    case 1:
        return QColor("orange");
    case 2:
        return QColor("yellow");
    case 3:
        return QColor("green");
    case 4:
        return QColor( "blue" );
    case 5:
        return QColor("indigo");
    case 6:
        return QColor("violet" );
    }

    return QColor("black");
}
