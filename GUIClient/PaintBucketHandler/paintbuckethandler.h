#ifndef PAINTBUCKETHANDLER_H
#define PAINTBUCKETHANDLER_H

#include <QObject>

class PaintBucketHandler : public QObject
{
    Q_OBJECT
public:
    explicit PaintBucketHandler(QObject *parent = nullptr);

signals:
    void finishedBucket( QString imageFile );

public slots:
    void UsePaintBucket( int CanvasWidth, int CanvasHeight, int XPos, int YPos, QString ImageFile, int NewR, int NewG, int NewB, bool isRainbow );
    QString filePath();
    void RemoveFiles();
private:
    QColor NewColor( int sel );
};

#endif // PAINTBUCKETHANDLER_H
