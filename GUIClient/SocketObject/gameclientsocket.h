#ifndef GAMECLIENTSOCKET_H
#define GAMECLIENTSOCKET_H

#include <QObject>
#include <QWebSocket>

#include "SocketObject/socketobjectbase.h"

class GameClientSocket : public SocketObjectBase
{
    Q_OBJECT
public:
    explicit GameClientSocket(QObject *parent = nullptr);
    ~GameClientSocket();
    void BeginConnection( QString );
    void SendSocketMessage( QString );
    void RegisterAlias( QString Alias );
    void ResendMessage();

signals:

private Q_SLOTS:
public slots:
    void RegisterAlias( QString OldAlias, QString NewAlias );

private:
    QWebSocket * clientWebSocket_;
    QString queuedMessage_;
};

#endif // GAMECLIENTSOCKET_H
