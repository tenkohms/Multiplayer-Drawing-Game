import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Item {

    anchors.fill: parent

    GameButton {
        id: backButton
        height: parent.height / 15
        width: parent.width / 15
        anchors.top: parent.top
        anchors.topMargin: 15
        anchors.left: parent.left
        anchors.leftMargin: 15

        defaultColor: "white"
        pressedColor: "grey"
        textColor: "#1e4c96"
        buttonText: "Back"
        onGo: gManager.SetNewLoaderPage( "mainMenuPage.qml" )
    }

    Text {
        id: title

        anchors.top: backButton.bottom
        anchors.topMargin: 10
        height: parent.height / 4
        width: parent.width
        horizontalAlignment: Text.AlignHCenter
        text: "Settings"
        color: "white"
        font.family:  fixedFont.name
        fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
    }
    Text {
        id: usernameLabel
        height: title.height / 2
        width: title.width / 2
        fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: title.bottom
        text: "Username"
        color: "white"
        font.family:  fixedFont.name
    }

    Text {
        id: username
        height: title.height / 2
        width: title.width / 2
        fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: usernameLabel.bottom
        text: gManager.GameSettingsUsername()
        color: "white"
        font.family:  fixedFont.name
    }
    GameButton
    {
        id: clearUsernameButton
        anchors.left: username.right
        anchors.leftMargin: 5
        anchors.verticalCenter: username.verticalCenter
        height: username.height
        width: backButton.width
        defaultColor: "#ef0909"
        pressedColor: "#ad0606"
        buttonText: "Clear"
        textColor: "white"
        onGo: {
            username.text = "Not Set"
            gManager.ClearGameSettingsUsername()
        }
    }

    Text {
        id: serverAddressLabel
        height: title.height / 2
        width: title.width / 2
        fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: username.bottom
        text: "Server Address:"
        color: "white"
        font.family:  fixedFont.name
    }

    Text {
        id: serverAddress
        height: title.height / 2
        width: title.width / 2
        fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: serverAddressLabel.bottom
        text: gManager.GameSettingsServerAddress()
        color: "white"
        font.family:  fixedFont.name
    }

    TextField {
        id: newServerAddress
        anchors.top: serverAddress.top
        anchors.horizontalCenter: serverAddress.horizontalCenter
        text: gManager.GameSettingsServerAddress()
        visible: false
        style: TextFieldStyle {
            background: Rectangle {
                color: "#143366"
            }
            textColor: "white"
        }

        font.pixelSize: 30
        font.family: fixedFont.name
    }

    GameButton
    {
        id: setServerAddressButton
        anchors.left: serverAddress.right
        anchors.leftMargin: 5
        anchors.verticalCenter: serverAddress.verticalCenter
        height: serverAddress.height
        width: backButton.width
        defaultColor: "#ef0909"
        pressedColor: "#ad0606"
        buttonText: "Set New"
        textColor: "white"
        onGo: {
            if ( newServerAddress.visible === false )
            {
                serverAddress.visible = false
                newServerAddress.visible = true
            }
            else
            {
                serverAddress.text = newServerAddress.text
                newServerAddress.visible = false
                serverAddress.visible = true
                gManager.SetGameSettingsServerAddress( newServerAddress.text )
            }
        }
    }


}
