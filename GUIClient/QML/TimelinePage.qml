import QtQuick 2.0
import QtQuick.Controls 1.4

Item {
    anchors.fill: parent

    ListModel {
        id: timelineList
        ListElement{ file: "/home/hpham/Desktop/origWord.png" }

        ListElement{ file: "/home/hpham/Desktop/origWord.png" }

        ListElement{ file: "/home/hpham/Desktop/origWord.png" }

        ListElement{ file: "/home/hpham/Desktop/origWord.png" }

        ListElement{ file: "/home/hpham/Desktop/origWord.png" }
    }

    Component {
        id: timelineDelegate

        Image {
            property bool hovered: false

            width: parent.width
            source: "file:///" + file
            fillMode: Image.PreserveAspectFit

            MouseArea {

                hoverEnabled: true

                anchors.fill: parent

                onEntered: {
                    parent.hovered = true
                }

                onExited: {
                    parent.hovered = false
                }

                onClicked: {

                }
            }

            Rectangle {
                color: Qt.rgba( 0, 0, 0, .9 )
                anchors.fill: parent
                visible: parent.hovered
            }
        }
    }

    ListView
    {
        anchors.fill: parent
        model: timelineList
        delegate: timelineDelegate
        spacing: 20

    }
}
