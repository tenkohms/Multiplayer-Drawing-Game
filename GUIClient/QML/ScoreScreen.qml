import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtMultimedia 5.8

import ScoreScreenWrapper 1.0

Item {
    anchors.fill: parent

    Audio {
        id: kidscheering
        source: "qrc:/Music/kidscheering.mp3"
        autoPlay: true
        loops: 1
        volume: 0.1
    }

    Text {
        id: title

        anchors.top: parent.top
        height: parent.height / 6
        width: parent.width
        horizontalAlignment: Text.AlignHCenter
        text: "Game Ovah"
        color: "white"
        font.family:  fixedFont.name
        fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
    }

    Text {
        anchors.top: title.bottom
        anchors.bottom: donePushButton.top
        anchors.topMargin: 5
        anchors.bottomMargin:5
        anchors.left:parent.left
        anchors.right: parent.right
        font.family:  fixedFont.name
        fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
        text: scorescreen.names
        color: "white"
        horizontalAlignment: Text.AlignHCenter
    }

    Rectangle {
        id: donePushButton
        height: parent.height / 15
        width: parent.width / 15
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 15
        anchors.horizontalCenter: parent.horizontalCenter
        color: "white"

        Text {
            height: parent.height
            width: parent.width * .8
            anchors.centerIn: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: "Done"
            color: "#1e4c96"
            font.family:  fixedFont.name
            fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
        }

        MouseArea {
            anchors.fill: parent
            onPressed: parent.color = "grey"
            onReleased: parent.color = "white"
            onClicked: scorescreen.Done()
        }
    }


}
