import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Extras 1.4
import QtQuick.Controls.Styles 1.4
import QtMultimedia 5.8
import QtQuick.Dialogs 1.2

import GameManager 1.0
import MainUiWrapper 1.0
import LoadingPageWrapper 1.0
import ScoreScreenWrapper 1.0

Window {
    visible: true
//    width: Screen.width
//    height: Screen.height

    width: 1000
    height: 675
    title: qsTr("Scribble")
    id: mainWindow
    visibility: Window.FullScreen

    Audio {
        id: bgMusic
        source: "qrc:/Music/Quirky Wacky.mp3"
        autoPlay: false
        loops: Audio.Infinite

        volume: .1
    }

    MessageDialog {
        id: errorDialog
        title: "Banned"
        text: "Your IP has been banned"
        onAccepted: {
            visible = false
        }
        Component.onCompleted: visible = false
    }

    Audio {
        id: doneNoise
        source: "qrc:/Music/audioclip-1503636482000-2432.wav"
        autoPlay: false
        loops: 1

        volume:0.2
    }

    property bool chatWindowShow : false
    property bool allowChat : false

    function showChatWindow() {
//        moverRect.visible = true
//        chatwindowtextarea.visible = true
//        chatwindowtextfield.visible = true
//        chatwindowSendMessageButton.visible = true;
//        closeChatButton.visible = true
//        chatWindowShow = true
    }

    function hideChatWindow()
    {
        moverRect.visible = false
        chatwindowtextarea.visible = false
        chatwindowtextfield.visible = false
        chatwindowSendMessageButton.visible = false;
        closeChatButton.visible = false
        chatWindowShow = false
    }

    Shortcut {
        sequence: "Ctrl+T"
        onActivated: {
            if ( !chatWindowShow && allowChat )
            {
                showChatWindow()
            }
        }
    }

    Component.onCompleted:
    {
        //showChatWindow()
        hideChatWindow()
        gManager.onNewNotification.connect( muiWrapper.receiveNewNotification )
    }

    FontLoader { id: fixedFont; source: "../Font/MB_PotatoesAndPeas.ttf" }
    FontLoader { id: guwul; source: "../Font/guwul.ttf" }

    Rectangle {
        anchors.fill: parent
        color: "#1e4c96"
    }

    GManager {
        id: gManager
        onBanError: errorDialog.visible = true
        onSetNewMainLoaderPage: {
            //audioIcon.visible = false
            muiWrapper.setLoaderPage( Page )
        }
        onNewChatMessage:
        {
            chatwindowtextarea.append(clientID + ": " + message );
        }
        onGuessesReceivedProgress: {
            loadingPage.setProgress( progress )
        }
        onGuessesTotalProgress: {
            loadingPage.setTotal( total )
        }
        onRoundProgress: {
            loadingPage.setRoundProgress( round )
        }
        onRoundTotal: {
            loadingPage.setRoundTotal( rTotal)
        }

        onScore: {
            scorescreen.setNames( scores )
        }

        onEnableChat: {
            allowChat = enable
        }

    }

    ScoreScreen{
        id: scorescreen
        onNewPage: {
            gManager.SetNewLoaderPage( Page )
        }
    }

    LoadingPage {
        id: loadingPage
        onPlaySound: doneNoise.play()
    }

    MUIWrapper {
        id: muiWrapper
        onNewNotification: {
            notificationMessageBarText.text = message
            notificationMessageBar.color = color
            notificationMessageBar.visible = true
        }
        onNewChatSend: {
            gManager.ChatMessageToSocketSlot( message )
        }
    }

    Loader {
        id: mainLoader
        anchors.fill: parent
        source: muiWrapper.loaderPage
        //source: "TestVotePage.qml"
    }


   ///////////////////--------Chat Window----------/////////////////////////

    property int mouseSX
    property int mouseSY
    property int newSX
    property int newSY

    Rectangle{
        id: moverRect
        width: chatwindowtextarea.width
        height: 20
        color: "black"

        function moveRect()
        {

            moverRect.x = moverRect.x + ( newSX - mouseSX )
            moverRect.y = moverRect.y + ( newSY - mouseSY )

            if ( moverRect.x + ( newSX - mouseSX ) < 0 )
            {
                moverRect.x = 0
            }

            if ( moverRect.x + ( newSX - mouseSX ) + moverRect.width > mainWindow.width )
            {
                moverRect.x = mainWindow.width - moverRect.width
            }

            if ( moverRect.y + ( newSY - mouseSY ) < 0 )
            {
                moverRect.y = 0;
            }

            if ( moverRect.y + ( newSY - mouseSY ) + moverRect.height > mainWindow.height )
            {
                moverRect.y = mainWindow.height - moverRect.height
            }

        }

        MouseArea {
            anchors.fill: parent
            onPressed: {
                mouseSX = mouseX
                mouseSY = mouseY
            }
            onPositionChanged: {
                newSX = mouseX
                newSY = mouseY
                moverRect.moveRect()
            }
        }
    }

    TextArea {
        id: chatwindowtextarea
        height: parent.height * .4
        width: parent.width * .4
        anchors.left: moverRect.left
        anchors.top: moverRect.bottom

        style: TextAreaStyle {
            backgroundColor: Qt.rgba( 0, 0, 0, 1)

            textColor: "white"
        }
        opacity: .5

        readOnly: true
    }

    TextField {
        id: chatwindowtextfield
        anchors.left: chatwindowtextarea.left
        anchors.top: chatwindowtextarea.bottom
        width: chatwindowtextarea.width * .75
        onAccepted: {
            muiWrapper.chatSlot( chatwindowtextfield.text )
            chatwindowtextfield.text = ""
        }

        maximumLength: 30

        style: TextFieldStyle {
            textColor: "white"
            background: Rectangle {
                color: Qt.rgba( 0, 0, 0, 0.5)
            }
        }

    }

    GameButton
    {
        id: chatwindowSendMessageButton

        anchors.left: chatwindowtextfield.right
        anchors.bottom: chatwindowtextfield.bottom
        anchors.top: chatwindowtextarea.bottom
        anchors.right: chatwindowtextarea.right

        defaultColor: Qt.rgba(0, 0, 0, .5)
        pressedColor: "grey"
        buttonText: "Send"
        textColor: "white"
        onGo:{
            muiWrapper.chatSlot( chatwindowtextfield.text )
            chatwindowtextfield.text = ""
        }
    }

    Rectangle {
        id: closeChatButton
        color: "red"
        height: moverRect.height
        width: height
        anchors.left: moverRect.left
        anchors.top: moverRect.top

        MouseArea {
            anchors.fill: parent
            onClicked: {
                hideChatWindow()
            }
        }
    }


    ///////////////////--------Chat Window----------/////////////////////////

    ///////////////////--------Host Control----------/////////////////////////

    Rectangle {
        visible: false
        id: hostControlBG
        anchors.centerIn: parent
        color: "#163a72"
        height: parent.height * .9
        width: parent.width * .9
        radius: 10

        Text {
            id: hostControlLabelText
            anchors.top: hostControlBG.top
            anchors.horizontalCenter: hostControlBG.horizontalCenter
            anchors.topMargin: 10
            height: parent.height / 5
            width: parent.width
            color: "white"
            font.family:  fixedFont.name
            fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
            horizontalAlignment: Text.AlignHCenter
            text: "Client disconnected"
        }

        GameButton {
            id: kickPlayer
            width: parent.width / 4
            height: parent.height / 10
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: hostControlLabelText.bottom
            anchors.topMargin: 10

            defaultColor: "#ef0909"
            pressedColor: "#ad0606"
            textColor: "white"
            buttonText: "Kick Player"
            onGo: gManager.KickPlayer()

        }

        Text {
            anchors.top: kickPlayer.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 10
            height: parent.height / 6
            width: parent.width
            text: "Or wait for rejoin..."

            color: "white"
            font.family:  fixedFont.name
            fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
            horizontalAlignment: Text.AlignHCenter
        }

    }



    ///////////////////--------Host Control----------/////////////////////////

    ///////////////////--------Notification Bar----------/////////////////////////
    Rectangle {
        id: notificationMessageBar
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width
        height: parent.height / 10

        visible: false

        onVisibleChanged: {
            if ( visible == true )
            {
                notificationMessageTimer.start()
            }
        }

        Text {
            id: notificationMessageBarText
            anchors.centerIn: parent
            width: parent.width
            height: parent.height
            fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 72
            horizontalAlignment: Text.AlignHCenter
            font.family: fixedFont.name
        }

        Timer {
            id: notificationMessageTimer
            interval: 2000
            onTriggered: {
                notificationMessageBar.visible = false
            }
        }

    }

    ///////////////////--------Notification Bar----------/////////////////////////

    property bool playingMusic : true

//    Image{
//        id: audioIcon

//        visible: false
//        source: {
//            if ( playingMusic === true ) {
//                "qrc:/Images/AudioIcon.png"
//            }
//            else
//            {
//                "qrc:/Images/NoAudioIcon.png"
//            }

//        }

//        anchors.bottom: parent.bottom
//        anchors.bottomMargin: 10
//        anchors.left: parent.left
//        anchors.leftMargin: 10
//        width: parent.width / 12
//        fillMode: Image.PreserveAspectFit
//        MouseArea {
//            anchors.fill: parent
//            onClicked: {
//                if ( playingMusic === true ) {
//                    playingMusic = false
//                    bgMusic.stop()
//                }
//                else
//                {
//                    playingMusic = true
//                    bgMusic.play()
//                }
//            }
//        }
//    }
}
