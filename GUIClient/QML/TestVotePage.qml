import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2

import VotePageWrapper 1.0

Item {
    anchors.fill: parent

    VotePage
    {
        id: votePage
    }

    Text {
        id: label
        anchors.top: parent.top
        anchors.topMargin: 10
        anchors.horizontalCenter: parent.horizontalCenter
        font.family: fixedFont.name
        color: "white"
        text: "Your word was: Some Random Phrase"
        font.pixelSize: 40
    }

    Rectangle {
        id: picBG
        anchors.top: label.bottom
        anchors.topMargin: 10
        anchors.horizontalCenter: parent.horizontalCenter

        height: 465
        width: 800
        color: "white"
    }

    Text {
        anchors.fill: picBG
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        text: "This is my guess..."
        fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 72
        font.family: guwul.name
    }

    Rectangle {
        id: nextPushButton
        height: parent.height / 15
        width: parent.width / 6

        anchors.right: parent.right
        anchors.rightMargin: 15
        anchors.top: picBG.bottom

        color: "#ffd905"

        Text {
            height: parent.height
            width: parent.width * .8
            anchors.centerIn: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: "Next"
            color: "#1e4c96"
            font.family:  fixedFont.name
            fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
        }

        MouseArea {
            anchors.fill: parent
            onPressed: parent.color = "#8e7900"
            onReleased: parent.color = "#ffd905"
            onClicked: gManager.StartJoin( hostTextField.text )
        }
    }

    Rectangle {
        id: backPushButton
        height: parent.height / 15
        width: parent.width / 6

        anchors.left: parent.left
        anchors.leftMargin: 15
        anchors.top: picBG.bottom

        color: "#ffd905"

        Text {
            height: parent.height
            width: parent.width * .8
            anchors.centerIn: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: "Previous"
            color: "#1e4c96"
            font.family:  fixedFont.name
            fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
        }

        MouseArea {
            anchors.fill: parent
            onPressed: parent.color = "#8e7900"
            onReleased: parent.color = "#ffd905"
            onClicked: gManager.StartJoin( hostTextField.text )
        }
    }

    Rectangle {
        id: selectPushButton
        height: parent.height / 15
        width: parent.width / 6

        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: picBG.bottom

        color: "#33c63f"

        Text {
            height: parent.height
            width: parent.width * .8
            anchors.centerIn: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: "Choose"
            color: "#1e4c96"
            font.family:  fixedFont.name
            fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
        }

        MouseArea {
            anchors.fill: parent
            onPressed: parent.color = "#218429"
            onReleased: parent.color = "#33c63f"
            onClicked: gManager.StartJoin( hostTextField.text )
        }
    }

    FileDialog {
        id: fileDialog
        title: "Please choose a folder"
        folder: shortcuts.home
        onAccepted: {
            console.log("You chose: " + fileDialog.fileUrls)
            Qt.quit()
        }
        onRejected: {
            console.log("Canceled")
            Qt.quit()
        }
        Component.onCompleted: visible = false
        selectFolder: true
        selectMultiple: false
    }

    Button {
        anchors.centerIn: parent
        text: "test"
        onClicked: fileDialog.visible = true
    }

}
