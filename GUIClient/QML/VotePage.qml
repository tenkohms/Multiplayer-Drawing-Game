import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2

import VotePageWrapper 1.0

Item {
    anchors.fill: parent

    property bool isOP: false

    function showButton()
    {
        //nextPushButton.visible = true;
        //backPushButton.visible = true;
        isOP = true
    }
    function hideButton()
    {
        nextPushButton.visible = false;
        backPushButton.visible = false;
        selectPushButton.visible = false;
    }

    VotePage
    {
        id: votePage
        onRequestGuess: gManager.GuessPageRoundRequest( Round )
        onChangeRound: gManager.fgQMLRoundChange( round )
        onGuessDataChanged: {
            if ( votePage.isImage === true )
            {
                imagePic.source = data
            }
            else
            {
                imagePic.source = ""
            }
        }

        onVoteSelect: {
            gManager.GuessPageVoteSelect( player )
        }

        onCurrentRoundChanged: {
            if ( ( round > 1) && ( isOP === true ) )
            {
                selectPushButton.visible = true
            }
            else
            {
                selectPushButton.visible = false
            }

            if ( ( votePage.currentRound > 0) && ( isOP === true ) )
            {
                backPushButton.visible = true;
            }
            else
            {
                backPushButton.visible = false;
            }

            if ( ( votePage.currentRound < rounds - 1 ) && ( isOP === true ) )
            {
                nextPushButton.visible = true;
            }
            else
            {
                nextPushButton.visible = false;
            }
        }
    }

    Component.onCompleted: {
        gManager.onFgIsImage.connect( votePage.setIsImage )
        gManager.onFgGuess.connect( votePage.setGuessData)
        gManager.onFgWord.connect( votePage.setOrigWord )
        gManager.onFgRounds.connect( votePage.setRounds )
        gManager.onFgCurrentPlayer.connect( votePage.setCurrentPlayer )
        gManager.onFgDisplayWord.connect( votePage.setDisplayWord)
        gManager.onFgChangeRound.connect( votePage.GetGuess)
        gManager.onIsOP.connect(showButton)
        gManager.onIsNotOP.connect(hideButton)
        mainWindow.showChatWindow()
    }

    Text {
        id: label
        anchors.top: parent.top
        anchors.topMargin: 10
        anchors.horizontalCenter: parent.horizontalCenter
        font.family: fixedFont.name
        color: "white"
        text: votePage.displayWord + ":"

        font.pixelSize: 40
    }

    Rectangle {
        id: picBG
        anchors.top: label.bottom
        anchors.topMargin: 10
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.leftMargin: 15
        anchors.rightMargin: 15
        anchors.bottom: selectPushButton.top
        color: "white"
    }

    Text {
        anchors.fill: picBG
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        text: votePage.guessData
        fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 72
        font.family: guwul.name
        visible: !votePage.isImage
    }

    Image {
        id: imagePic
        anchors.fill: picBG
        visible: votePage.isImage
        source: votePage.guessData
        fillMode: Image.PreserveAspectFit
    }

    Rectangle {
        id: nextPushButton
        height: parent.height / 15
        width: parent.width / 6

        anchors.right: parent.right
        anchors.rightMargin: 15
        anchors.top: selectPushButton.top

        color: "#ffd905"

        Text {
            height: parent.height
            width: parent.width * .8
            anchors.centerIn: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: "Next"
            color: "white"
            font.family:  fixedFont.name
            fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
        }

        MouseArea {
            anchors.fill: parent
            onPressed: parent.color = "#8e7900"
            onReleased: parent.color = "#ffd905"
            onClicked: votePage.changeRoundSlot(1)
        }
    }

    Rectangle {
        id: backPushButton
        height: parent.height / 15
        width: parent.width / 6

        anchors.left: parent.left
        anchors.leftMargin: 15
        anchors.top: selectPushButton.top
        visible: false

        color: "#ffd905"

        Text {
            height: parent.height
            width: parent.width * .8
            anchors.centerIn: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: "Previous"
            color: "white"
            font.family:  fixedFont.name
            fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
        }

        MouseArea {
            anchors.fill: parent
            onPressed: parent.color = "#8e7900"
            onReleased: parent.color = "#ffd905"
            onClicked: votePage.changeRoundSlot(-1)
        }
    }

    Rectangle {
        id: selectPushButton
        height: parent.height / 15
        width: parent.width / 6

        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10

        color: "#33c63f"
        visible: false

        Text {
            height: parent.height
            width: parent.width * .8
            anchors.centerIn: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: "Choose"
            color: "#1e4c96"
            font.family:  fixedFont.name
            fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
        }

        MouseArea {
            anchors.fill: parent
            onPressed: parent.color = "#218429"
            onReleased: parent.color = "#33c63f"
            onClicked: votePage.voteSelectSlot()
        }
    }

}
