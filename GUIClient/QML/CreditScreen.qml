import QtQuick 2.0

Item {
    anchors.fill: parent
    GameButton {
        id: backButton
        height: parent.height / 15
        width: parent.width / 15
        anchors.top: parent.top
        anchors.topMargin: 15
        anchors.left: parent.left
        anchors.leftMargin: 15

        defaultColor: "white"
        pressedColor: "grey"
        textColor: "#1e4c96"
        buttonText: "Back"
        onGo: gManager.SetNewLoaderPage( "HostOrJoin.qml" )
    }
    Text {
        id: title

        anchors.top: backButton.bottom
        anchors.topMargin: 10
        height: parent.height / 2.5
        width: parent.width
        horizontalAlignment: Text.AlignHCenter
        text: "Credits"
        color: "white"
        font.family:  fixedFont.name
        fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
    }
    Text {
        id: usernameLabel
        height: title.height / 2
        width: title.width / 2
        fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: title.bottom
        text: "David Maciel - File input code"
        color: "white"
        font.family:  fixedFont.name
    }
    Text {
        id: username
        height: title.height / 2
        width: title.width / 2
        fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: usernameLabel.bottom
        text: "Matt Smith - Placeholder"
        color: "white"
        font.family:  fixedFont.name
    }
    Text {
        id: usernames
        height: title.height / 2
        width: title.width / 2
        fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: username.bottom
        text: "John Smith - Main Dev"
        color: "white"
        font.family:  fixedFont.name
    }
}
