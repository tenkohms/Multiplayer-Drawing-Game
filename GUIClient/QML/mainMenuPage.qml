import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3

Item {
    anchors.fill: parent
    id: mmPage
    Image {
        anchors.fill: parent
        fillMode: Image.PreserveAspectFit
        source: "qrc:/Images/turtleSplash.png"
    }

    GameButton {
        id: playButton
        height: parent.height / 15
        width: parent.width / 3
        anchors.left: parent.left
        anchors.leftMargin: 20
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 15

        defaultColor: "#ffd905"
        pressedColor: "#8e7900"
        textColor: "white"
        buttonText: "Play"
        onGo: {
            gManager.ConnectToHost()
            playButton.buttonText = "Connecting..."
            playButton.enabled = false
        }
    }

    GameButton {
        id: settingsButton
        height: parent.height / 15
        width: parent.width / 3
        anchors.right: parent.right
        anchors.rightMargin: 20
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 15

        defaultColor: "#33c63f"
        pressedColor: "#218429"
        textColor: "white"
        buttonText: "Settings"
        onGo: gManager.SetNewLoaderPage( "GameSettings.qml")
    }


    Text {
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        height: parent.height / 15
        width: parent.width / 15
        text: "v0.8.0"
        color: "white"
        font.family:  fixedFont.name
        fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
    }

}
