import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import SetAliasWrapper 1.0

Item {

    SAWrapper {
        id: setAliasWrapper
        onFailedAliasSetChanged: {
            setUsernamePushButton.buttonEnabled = true
            setAliasWrapper.setReadyForNext( true );
        }

        onNewAlias: {
            gManager.RegisterAliasSlot( alias )
        }
        onEnableButton: setUsernamePushButton.buttonEnabled = true
        onAliasSet: gManager.SetNewLoaderPage( "HostOrJoin.qml" )
    }

    Component.onCompleted: {
        gManager.onAliasSetFailed.connect( setAliasWrapper.setFailedAliasSet )
        setAliasWrapper.onChangeLoader.connect( gManager.SetNewLoaderPage )
    }

    GameButton {
        id: setUsernamePushButton
        height: parent.height / 15
        width: usernameTextField.width / 2
        anchors.top: usernameTextField.bottom
        anchors.topMargin: 15
        anchors.horizontalCenter: parent.horizontalCenter

        defaultColor: "white"
        pressedColor: "grey"
        textColor: "#1e4c96"
        buttonText: "Set Username"
        onGo: {
            setUsernamePushButton.buttonEnabled = false
            setAliasWrapper.setReadyForNext( true );
            setAliasWrapper.validateAndSendAlias( usernameTextField.text )
        }
    }

    TextField {
        id: usernameTextField
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.centerIn: parent
        width: parent.width / 2
        height: parent.height / 8
        horizontalAlignment: Text.AlignHCenter
        placeholderText: "Username"
        style: TextFieldStyle {
            background: Rectangle {
                color: "#143366"
            }
            textColor: "white"
        }
        maximumLength: 15

        font.pixelSize: 30 * ( parent.height / 675)
        font.family: fixedFont.name
    }

    Label {
        id: failedaliassetLabel
        anchors.bottom: usernameTextField.top
        anchors.bottomMargin: 15
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width
        height: usernameTextField.height
        Text {
            anchors.fill: parent
            color: "red"
            text: "Username not available"
            horizontalAlignment: Text.AlignHCenter
        }
        visible: setAliasWrapper.failedAliasSet

        onVisibleChanged: {
            if ( visible == true )
            {
                setUsernamePushButton.buttonEnabled = true
                setUsernamePushButton.color = "white"
                setAliasWrapper.setReadyForNext( true );
            }
        }
    }
}
