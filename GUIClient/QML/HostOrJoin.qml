import QtQuick 2.0
import QtQuick.Layouts 1.3
Item {

    anchors.fill: parent

    GameButton {
        id: backButton
        height: parent.height / 15
        width: parent.width / 15
        anchors.top: parent.top
        anchors.topMargin: 15
        anchors.left: parent.left
        anchors.leftMargin: 15

        defaultColor: "white"
        pressedColor: "grey"
        textColor: "#1e4c96"
        buttonText: "Back"
        onGo: gManager.SocketObjectDisconnected()
    }

    GameButton{
        id: hostGameButton
        height: parent.height / 5
        width: parent.width / 3

        anchors.centerIn: parent
        defaultColor: "#ffd905"
        pressedColor: "#8e7900"
        textColor: "white"
        buttonText: "Host Game"
        onGo: gManager.HostNewGame()

    }

    GameButton {
        id: joinGameButton
        height: parent.height / 5
        width: parent.width / 3
        anchors.horizontalCenter: hostGameButton.horizontalCenter
        anchors.top: hostGameButton.bottom
        anchors.topMargin: 15

        defaultColor: "#33c63f"
        pressedColor: "#218429"
        textColor: "white"
        buttonText: "Join Game"
        onGo: gManager.SetNewLoaderPage( "connectToGame.qml" )
    }
}
