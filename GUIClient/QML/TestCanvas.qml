import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2
import QtGraphicalEffects 1.0

import CanvasPageWrapper 1.0
import PaintBucketHandler 1.0

Item {
    anchors.fill: parent

    property bool filled: false

    property int startX
    property int startY
    property int finishX
    property int finishY

    property string brushColor : "black"
    property string lastCustomColor
    property int brushSize: 6
    property string custom: "#1e4c96"

    property bool fillTool: false
    property bool eraserTool: false
    property bool brushTool: true
    property bool rainbowBrush: false
    property bool isBucketDone: true
    property bool isUndo : true
    property string pbImage

    property var rainbow : [ "red", "orange", "yellow", "green", "blue", "#4b0082", "#8a2be2"]
    property int ticker : 0
    property int rainbowSel: 0

    CanvasWrapper {
        id: canvasWrapper
        onImageFileData: {
            gManager.ReceiveImageFileData( fileData )
        }
        onSetUIPage: {
            gManager.SetNewLoaderPage( Page )
        }
    }

    PaintBucket {
        id: paintbucket
        onFinishedBucket: {
            isUndo = false
            pbImage = imageFile.toString()
            myCanvas.loadImage( imageFile )
        }
    }

    Component.onCompleted: {
        gManager.onNewRoundWord.connect( canvasWrapper.setRoundWord )
        mainWindow.hideChatWindow()
    }

    ////------------Functions-----------------//////
    function saveCanvas()
    {
        paintbucket.RemoveFiles();
        myCanvas.save( canvasWrapper.imageFileLocation + "/imageFile.png")
        canvasWrapper.setUI("LoadingPage.qml")
        canvasWrapper.DrawingTimeOver()
    }

    function setColor( select, setBrush )
    {
        listview.currentIndex = select
        brushColor = colorList.get( select ).name
        if ( eraserTool )
        {
            fillTool = false
            brushTool = true
            paintBucketBG.color = "#143366"
            paintBrushBG.color = "grey"
            eraserTool = false
            eraserButtonBG.color = "#143366"
        }

        if ( setBrush )
        {
            fillTool = false
            brushTool = true
            paintBucketBG.color = "#143366"
            paintBrushBG.color = "grey"
            eraserTool = false
            eraserButtonBG.color = "#143366"
        }

        if ( select === 9 )
        {
            rainbowBrush = true;
        }
    }

    Shortcut {
        sequence: "Ctrl+1"
        onActivated: {
            setColor( 0, true )
        }
    }

    Shortcut {
        sequence: "Ctrl+2"
        onActivated: {
            setColor( 1, true )
        }
    }

    Shortcut {
        sequence: "Ctrl+3"
        onActivated: {
            setColor( 2 , true)
        }
    }

    Shortcut {
        sequence: "Ctrl+4"
        onActivated: {
            setColor( 3, true )
        }
    }

    Shortcut {
        sequence: "Ctrl+5"
        onActivated: {
            setColor( 4, true )
        }
    }

    Shortcut {
        sequence: "Ctrl+6"
        onActivated: {
            setColor( 5 , true)
        }
    }

    Shortcut {
        sequence: "Ctrl+7"
        onActivated: {
            setColor( 6, true )
        }
    }

    Shortcut {
        sequence: "Ctrl+8"
        onActivated: {
            setColor( 7, true )
        }
    }

    Shortcut {
        sequence: "Ctrl+9"
        onActivated: {
            setColor( 8, true )
        }
    }

    Shortcut {
        sequence: "Ctrl+0"
        onActivated: {
            setColor( 9, true )
        }
    }

    Shortcut {
        sequence: "1"
        onActivated: {
            setColor( 0, false )
        }
    }

    Shortcut {
        sequence: "2"
        onActivated: {
            setColor( 1, false )
        }
    }

    Shortcut {
        sequence: "3"
        onActivated: {
            setColor( 2, false )
        }
    }

    Shortcut {
        sequence: "4"
        onActivated: {
            setColor( 3, false )
        }
    }

    Shortcut {
        sequence: "5"
        onActivated: {
            setColor( 4, false )
        }
    }

    Shortcut {
        sequence: "6"
        onActivated: {
            setColor( 5, false )
        }
    }

    Shortcut {
        sequence: "7"
        onActivated: {
            setColor( 6, false )
        }
    }

    Shortcut {
        sequence: "8"
        onActivated: {
            setColor( 7, false )
        }
    }

    Shortcut {
        sequence: "9"
        onActivated: {
            setColor( 8, false )
        }
    }

    Shortcut {
        sequence: "0"
        onActivated: {
            setColor( 9, false )
        }
    }




    Shortcut {
        id: undoShortcut
        sequence: "Ctrl+Z"
        onActivated: {
            myCanvas.undo()
        }
    }

    Shortcut {
        id: closeDialog
        sequence: "Ctrl+W"
        onActivated: {
            if ( replaceColor.visible == true )
            {
                replaceColor.visible = false;
            }
        }
    }

    Shortcut {
        id: brushShortcut
        sequence: "Ctrl+V"
        onActivated: {
            if ( brushTool )
            {
                brushSizeSelectorBG.visible = !brushSizeSelectorBG.visible
            }

            if ( !brushTool )
            {
                fillTool = false
                brushTool = true
                paintBucketBG.color = "#143366"
                paintBrushBG.color = "grey"
                eraserTool = false
                eraserButtonBG.color = "#143366"
            }
        }
    }
    Shortcut {
        id: bucketShortcut
        sequence: "Ctrl+C"
        onActivated: {
            fillTool = true
            brushTool = false
            eraserTool = false
            eraserButtonBG.color = "#143366"
            paintBucketBG.color = "grey"
            paintBrushBG.color = "#143366"
        }
    }
    Shortcut {
        id: eraseShortCut
        sequence: "Ctrl+X"
        onActivated: {
            if ( eraserTool )
            {
                eraserToolSizeSelector.visible = !eraserToolSizeSelector.visible
            }

            if ( !eraserTool )
            {
                fillTool = false
                brushTool = false
                paintBucketBG.color = "#143366"
                paintBrushBG.color = "#143366"
                eraserTool = true
                eraserButtonBG.color = "grey"
            }
        }
    }

    MessageDialog {
        id: confirmDialog
        title: "Submit Drawing"
        text: "Do you want to send the drawing?"
        standardButtons: StandardButton.Yes | StandardButton.No
        onYes: saveCanvas()
        onNo: confirmDialog.visible = false
        Component.onCompleted: visible = false
    }


    ColorDialog {
        id: colorDialog
        visible: false
        title: "Please choose a color"
        onAccepted: {
            visible = false
        }
        onRejected: {
            visible = false
        }
        onCurrentColorChanged: {
            brushColor = colorDialog.currentColor
            custom = colorDialog.currentColor
            lastCustomColor = colorDialog.currentColor
        }
    }

    ColorDialog {
        id: replaceColor
        visible: false
        title: "Please choose a color"
        onAccepted: {
            changeColorHolder.text = color
            brushColor = color
            colorList.setProperty( listview.currentIndex, "name", changeColorHolder.text )
            visible = false
        }
        onRejected: {
            visible = false
        }

        onCurrentColorChanged: {
            changeColorHolder.text = currentColor
            brushColor = currentColor
            colorList.setProperty( listview.currentIndex, "name", changeColorHolder.text )
        }
    }

    Text {
        id: changeColorHolder
        visible: false
    }

    Text {
        id: word

        anchors.top: parent.top
        anchors.topMargin: 10
        height: parent.height / 8
        width: parent.width
        horizontalAlignment: Text.AlignHCenter
        text: canvasWrapper.roundWord
        color: "white"
        font.family:  fixedFont.name
        fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
    }

    Rectangle {
        id: toolbarRect

        anchors.top: word.bottom
        anchors.topMargin: 10
        height: parent.height / 15
        width: parent.width
        color: "#143366"
    }

    ListModel{
        id: colorList
        ListElement{ name: "black" ; selectable: true; isRainbow: false }
        ListElement{ name: "red" ; selectable: true; isRainbow: false }
        ListElement{ name: "green" ; selectable: true; isRainbow: false }
        ListElement{ name: "blue" ; selectable: true; isRainbow: false }
        ListElement{ name: "#774d00" ; selectable: true; isRainbow: false }
        ListElement{ name: "yellow" ; selectable: true; isRainbow: false }
        ListElement{ name: "magenta" ; selectable: true; isRainbow: false }
        ListElement{ name: "gray" ; selectable: true; isRainbow: false }
        ListElement{ name: "white" ; selectable: true; isRainbow: false }
        ListElement{ name: "rainbow" ; selectable: true; isRainbow: true }
    }

    Component {
        id: colorDelegate
        Rectangle{
            height: parent.height * .75
            width: height
            anchors.verticalCenter: parent.verticalCenter
            color: {
                if ( !isRainbow )
                {
                    name
                }
                else
                {
                    "white"
                }
            }

            Image {
                id: colorWheel
                anchors.fill: parent
                source: "qrc:/Images/rainwbowSquare.png"

                fillMode: Image.PreserveAspectFit

                visible:  isRainbow
            }

            MouseArea {

                acceptedButtons: Qt.LeftButton | Qt.RightButton
                anchors.fill: parent
                onDoubleClicked: {
                    brushColor = name
                    listview.currentIndex = index
                    fillTool = false
                    brushTool = true
                    paintBucketBG.color = "#143366"
                    paintBrushBG.color = "grey"
                    eraserTool = false
                    eraserButtonBG.color = "#143366"
                }

                onClicked: {
                    if ( mouse.button === Qt.RightButton )
                    {
                        if ( !isRainbow )
                        {
                            listview.currentIndex = index
                            replaceColor.visible = true
                        }
                    }
                    else
                    {
                        brushColor = name
                        if ( selectable === true ) {
                            listview.currentIndex = index
                        }
                        if ( isRainbow ){
                            rainbowBrush = true
                        }
                        if ( !isRainbow )
                        {
                            rainbowBrush = false
                        }

                        if ( eraserTool )
                        {
                            fillTool = false
                            brushTool = true
                            paintBucketBG.color = "#143366"
                            paintBrushBG.color = "grey"
                            eraserTool = false
                            eraserButtonBG.color = "#143366"
                        }
                    }
                }
            }
        }
    }


    ListView
    {
        id: listview
        height: toolbarRect.height
        width: parent.width
        anchors.left: clearButton.right
        anchors.top: toolbarRect.top
        anchors.leftMargin: 15

        model: colorList
        orientation: Qt.Horizontal
        flickableDirection: Flickable.AutoFlickDirection
        spacing: 20

        delegate: colorDelegate
        clip: true

        highlight: Rectangle {
            height: toolbarRect.height * 2
            color: "grey"
        }
    }

    GameButton {
        id: submitButton
        height: toolbarRect.height
        width: parent.width / 12
        anchors.top: toolbarRect.top
        anchors.right: parent.right

        defaultColor: "#33c63f"
        pressedColor: "#218429"
        textColor: "white"
        buttonText: "Done"
        onGo: confirmDialog.visible = true
    }

    Image {
        id: undoButton
        height: toolbarRect.height
        width: parent.width / 15
        anchors.top: toolbarRect.top
        anchors.right: submitButton.left
        anchors.rightMargin: 10
        fillMode: Image.PreserveAspectFit

        source: "qrc:/Images/Undo.png"

        MouseArea {
            anchors.fill: parent
            onClicked: myCanvas.undo()
        }

    }

    GameButton {
        id: clearButton

        height: toolbarRect.height
        width: parent.width / 15
        anchors.top: toolbarRect.top
        anchors.left: toolbarRect.left

        defaultColor: "#ef0909"
        pressedColor: "#ad0606"
        textColor: "white"
        buttonText: "Clear"
        onGo: {
            myCanvas.clearCanvas()
        }
    }

    Rectangle {
        id: eraserButtonBG
        anchors.fill: eraserButton
        color: "#143366"
    }

    Image {
        id: eraserButton
        anchors.right: bucket.left
        anchors.rightMargin: 5
        anchors.top: toolbarRect.top
        anchors.bottom: toolbarRect.bottom
        source: "qrc:/Images/eraser.png"
        fillMode: Image.PreserveAspectFit

    }

    MouseArea {
        anchors.fill: eraserButton
        onClicked: {
            if ( eraserTool )
            {
                eraserToolSizeSelector.visible = !eraserToolSizeSelector.visible
            }

            if ( !eraserTool )
            {
                fillTool = false
                brushTool = false
                paintBucketBG.color = "#143366"
                paintBrushBG.color = "#143366"
                eraserTool = true
                eraserButtonBG.color = "grey"
            }
        }
    }

    Rectangle {
        id: paintBucketBG
        anchors.fill: bucket
        color: "#143366"
    }

    Image {
        id: bucket
        anchors.right: paintBrushColor.left
        anchors.rightMargin: 5
        anchors.top: toolbarRect.top
        anchors.bottom: toolbarRect.bottom
        source: "qrc:/Images/Paint.png"
        fillMode: Image.PreserveAspectFit

    }


    ColorOverlay {
            anchors.fill: bucket
            source: bucket
            color: brushColor
        }

    Image {
        id: paintBucketBody
        anchors.fill: bucket
        source: "qrc:/Images/paintBucketBody.png"
        fillMode: Image.PreserveAspectFit
    }

    MouseArea {
        anchors.fill: paintBucketBody
        onClicked: {

            fillTool = true
            brushTool = false
            eraserTool = false
            eraserButtonBG.color = "#143366"
            paintBucketBG.color = "grey"
            paintBrushBG.color = "#143366"

        }
    }

    Rectangle {
        id: paintBrushBG
        anchors.fill: paintBrushColor
        color: "grey"
    }

    Image {
        id: paintBrushColor
        anchors.right: undoButton.left
        anchors.rightMargin: 5
        anchors.top: toolbarRect.top
        anchors.bottom: toolbarRect.bottom
        source: "qrc:/Images/paintBrushColor.png"
        fillMode: Image.PreserveAspectFit
    }

    ColorOverlay{
        anchors.fill: paintBrushColor
        source: paintBrushColor
        color: brushColor
    }



    Image {
        id: paintBrushBody
        anchors.fill: paintBrushColor
        source: "qrc:/Images/paintBrushBody.png"
        fillMode: Image.PreserveAspectFit
    }

    MouseArea {
        anchors.fill: paintBrushBody
        onClicked: {

            if ( brushTool )
            {
                brushSizeSelectorBG.visible = !brushSizeSelectorBG.visible
            }

            if ( !brushTool )
            {
                fillTool = false
                brushTool = true
                paintBucketBG.color = "#143366"
                paintBrushBG.color = "grey"
                eraserTool = false
                eraserButtonBG.color = "#143366"
            }
        }
    }

    Text {
        id: colorHolder
        visible: false
        color: brushColor
    }

    Rectangle {
        anchors.fill: myCanvas
        color: "white"
    }

    property var tempData
    Canvas {
        id: myCanvas
        anchors.top: toolbarRect.bottom
        anchors.bottom: progressStatus.top
        anchors.bottomMargin: 5
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.leftMargin: 10
        anchors.rightMargin: 10

        property variant holder: []
        property var undoImage;
        property var redoImage;
        property int holderIndex: -1;

        onWidthChanged: {
            if ( myCanvas.available )
            {
                if ( holderIndex > -1) {

                    var ctx = myCanvas.getContext("2d")
                    ctx.drawImage(holder[ holderIndex ], 0, 0)
                    myCanvas.requestPaint()

                }
            }
        }
        onHeightChanged: {
            if ( myCanvas.available )
            {
                if ( holderIndex > -1) {

                    var ctx = myCanvas.getContext("2d")
                    ctx.drawImage(holder[ holderIndex ], 0, 0)
                    myCanvas.requestPaint()

                }
            }
        }

        function saveTemp()
        {
            var ctx = myCanvas.getContext("2d")
            var layer = ctx.getImageData(0, 0, myCanvas.width, myCanvas.height)
            tempData = layer
        }

        function cPush() {

            holderIndex++;
            holder[ holderIndex ] = myCanvas.toDataURL()
        }

        function undo() {
            if ( holderIndex - 1 > -1 )
            {
                myCanvas.unloadImage( undoImage )
                isUndo = true
                undoImage = holder[ holderIndex - 1]
                myCanvas.loadImage( undoImage )
                holderIndex--
            }
        }

        function clearCanvas() {
            cPush()
            startX = -1;
            startY = -1;
            finishX = -1;
            finishY = -1;
            var ctx = getContext("2d");
            ctx.globalAlpha = myCanvas.alpha;
            ctx.fillStyle = "white";
            ctx.fillRect(0, 0, myCanvas.width, myCanvas.height);
            myCanvas.requestPaint();
        }

        onImageLoaded: {
            var ctx = getContext("2d")
            ctx.globalAlpha = myCanvas.alpha;
            ctx.fillStyle = "white";
            ctx.fillRect(0, 0, myCanvas.width, myCanvas.height);
            if ( isUndo ) {
                ctx.drawImage( undoImage, 0, 0)
            }
            if ( !isUndo )
            {
                ctx.drawImage( pbImage, 0, 0)
                cPush()
            }

            startX = -1;
            startY = -1;
            finishX = -1;
            finishY = -1;
            myCanvas.requestPaint()
        }

        onPaint: {
            if ( !filled )
            {
                var ctx = getContext("2d");
                ctx.globalAlpha = myCanvas.alpha;
                ctx.fillStyle = "white";
                ctx.fillRect(0, 0, myCanvas.width, myCanvas.height);
                filled = true;
                cPush()
            }
            if( brushTool && !rainbowBrush)
            {
                var ctx = getContext('2d')
                ctx.beginPath();
                ctx.strokeStyle = brushColor;
                ctx.lineWidth = brushSize;
                ctx.lineJoin = "round";
                ctx.moveTo(startX, startY);
                ctx.lineTo(finishX, finishY);
                ctx.closePath();
                ctx.stroke();
                startX = finishX;
                startY = finishY;
            }
            if ( rainbowBrush && !fillTool)
            {
                ticker++
                if ( ticker >= 1)
                {
                    ticker = 0
                    rainbowSel++
                }

                if ( rainbowSel > 6 )
                {
                    rainbowSel = 0
                }

                var ctx = getContext('2d')
                ctx.beginPath();
                ctx.strokeStyle = rainbow[rainbowSel];
                ctx.lineWidth = brushSize;
                ctx.lineJoin = "round";
                ctx.moveTo(startX, startY);
                ctx.lineTo(finishX, finishY);
                ctx.closePath();
                ctx.stroke();
                startX = finishX;
                startY = finishY;
            }

            if ( eraserTool )
            {
                var ctx = getContext('2d')
                ctx.beginPath();
                ctx.strokeStyle = "white";
                ctx.lineWidth = brushSize;
                ctx.lineJoin = "round";
                ctx.moveTo(startX, startY);
                ctx.lineTo(finishX, finishY);
                ctx.closePath();
                ctx.stroke();
                startX = finishX;
                startY = finishY;
            }

            if ( fillTool )
            {
                isBucketDone = true
            }
        }
    }




    MouseArea {
        anchors.fill: myCanvas
        onPressed: {
            startX = mouseX
            startY = mouseY
        }
        onPositionChanged: {
            finishX = mouseX
            finishY = mouseY
            myCanvas.requestPaint()
        }
        onReleased: {
            ticker = 0
            rainbowSel = 0
            if ( brushTool )
            {
                myCanvas.cPush()
            }
        }

        onClicked: {
            if ( fillTool && isBucketDone )
            {
                isBucketDone = false
                myCanvas.save(paintbucket.filePath())
                startX = mouseX
                startY = mouseY
                myCanvas.unloadImage(pbImage)
                paintbucket.UsePaintBucket( myCanvas.width,myCanvas.height,startX, startY, paintbucket.filePath(), colorHolder.color.r * 255, colorHolder.color.g * 255, colorHolder.color.b * 255, rainbowBrush)

            }
        }
    }

    Text {
        id: progressStatus
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 5
        anchors.horizontalCenter: parent.horizontalCenter
        color: "white"
        font.family: fixedFont.name
        height: parent.height / 15
        horizontalAlignment: Text.AlignHCenter
        fontSizeMode: Text.VerticalFit; minimumPixelSize: 10; font.pixelSize: 72
        text: loadingPage.progress + " / " + loadingPage.total + " players ready"
    }

    Rectangle {
        id: eraserToolSizeSelector

        height: eraserButton.height * 3
        width: eraserButton.width

        anchors.top: eraserButtonBG.bottom
        anchors.horizontalCenter: eraserButtonBG.horizontalCenter

        visible: false

        color: "grey"

        Rectangle {
            id: smallEraserBrush
            height: parent.height / 3
            width: parent.width
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter

            color: "black"

            Rectangle {
                anchors.centerIn: parent
                color: "white"

                width: 6
                height: 6
                radius: 3

                border.color: "white"
                border.width: 1
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    smallBrush.color = "black"
                    mediumBrush.color = "grey"
                    largeBrush.color = "grey"
                    smallEraserBrush.color = "black"
                    mediumEraserBrush.color = "grey"
                    largeEraserBrush.color = "grey"
                    brushSize = 6
                    brushSizeSelectorBG.visible = false
                    eraserToolSizeSelector.visible = false
                }
            }
        }

        Rectangle {
            id: mediumEraserBrush
            height: parent.height / 3
            width: parent.width
            anchors.top: smallEraserBrush.bottom
            anchors.horizontalCenter: parent.horizontalCenter

            color: "grey"

            Rectangle {
                anchors.centerIn: parent
                color: "white"

                width: 12
                height: 12
                radius: 6

                border.color: "white"
                border.width: 1
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    smallBrush.color = "grey"
                    mediumBrush.color = "black"
                    largeBrush.color = "grey"
                    smallEraserBrush.color = "grey"
                    mediumEraserBrush.color = "black"
                    largeEraserBrush.color = "grey"
                    brushSize = 12
                    brushSizeSelectorBG.visible = false
                    eraserToolSizeSelector.visible = false
                }
            }
        }

        Rectangle {
            id: largeEraserBrush
            height: parent.height / 3
            width: parent.width
            anchors.top: mediumEraserBrush.bottom
            anchors.horizontalCenter: parent.horizontalCenter

            color: "grey"

            Rectangle {
                anchors.centerIn: parent
                color: "white"

                width: 24
                height: 24
                radius: 12

                border.color: "white"
                border.width: 1
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    smallBrush.color = "grey"
                    mediumBrush.color = "grey"
                    largeBrush.color = "black"
                    smallEraserBrush.color = "grey"
                    mediumEraserBrush.color = "grey"
                    largeEraserBrush.color = "black"
                    brushSize = 24
                    brushSizeSelectorBG.visible = false
                    eraserToolSizeSelector.visible = false
                }
            }
        }
    }

    Rectangle {
        id: brushSizeSelectorBG

        height: paintBrushColor.height * 3
        width: paintBrushColor.width

        anchors.top: paintBrushColor.bottom
        anchors.horizontalCenter: paintBrushColor.horizontalCenter

        visible: false

        color: "grey"

        Rectangle {
            id: smallBrush
            height: parent.height / 3
            width: parent.width
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter

            color: "black"

            Rectangle {
                anchors.centerIn: parent
                color: brushColor

                width: 6
                height: 6
                radius: 3

                border.color: "white"
                border.width: 1
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    smallBrush.color = "black"
                    mediumBrush.color = "grey"
                    largeBrush.color = "grey"
                    smallEraserBrush.color = "black"
                    mediumEraserBrush.color = "grey"
                    largeEraserBrush.color = "grey"
                    brushSize = 6
                    brushSizeSelectorBG.visible = false
                    eraserToolSizeSelector.visible = false
                }
            }
        }

        Rectangle {
            id: mediumBrush
            height: parent.height / 3
            width: parent.width
            anchors.top: smallBrush.bottom
            anchors.horizontalCenter: parent.horizontalCenter

            color: "grey"

            Rectangle {
                anchors.centerIn: parent
                color: brushColor

                width: 12
                height: 12
                radius: 6

                border.color: "white"
                border.width: 1
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    smallBrush.color = "grey"
                    mediumBrush.color = "black"
                    largeBrush.color = "grey"
                    smallEraserBrush.color = "grey"
                    mediumEraserBrush.color = "black"
                    largeEraserBrush.color = "grey"
                    brushSize = 12
                    brushSizeSelectorBG.visible = false
                    eraserToolSizeSelector.visible = false
                }
            }
        }

        Rectangle {
            id: largeBrush
            height: parent.height / 3
            width: parent.width
            anchors.top: mediumBrush.bottom
            anchors.horizontalCenter: parent.horizontalCenter

            color: "grey"

            Rectangle {
                anchors.centerIn: parent
                color: brushColor

                width: 24
                height: 24
                radius: 12

                border.color: "white"
                border.width: 1
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    smallBrush.color = "grey"
                    mediumBrush.color = "grey"
                    largeBrush.color = "black"
                    smallEraserBrush.color = "grey"
                    mediumEraserBrush.color = "grey"
                    largeEraserBrush.color = "black"
                    brushSize = 24
                    brushSizeSelectorBG.visible = false
                    eraserToolSizeSelector.visible = false
                }
            }
        }
    }

}
