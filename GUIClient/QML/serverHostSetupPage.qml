import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Item {
    anchors.fill: parent

    Rectangle {
        id: backButton
        height: parent.height / 15
        width: parent.width / 15
        anchors.top: parent.top
        anchors.topMargin: 15
        anchors.left: parent.left
        anchors.leftMargin: 15
        color: "white"

        Text {
            height: parent.height
            width: parent.width
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: "Back"
            color: "#1e4c96"
            font.family:  fixedFont.name
            fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
        }

        MouseArea {
            anchors.fill: parent
            onPressed: parent.color = "grey"
            onReleased: parent.color = "white"
            onClicked: muiWrapper.setLoaderPage( "mainMenuPage.qml" )
        }
    }

    Text {
        id: title

        anchors.top: backButton.bottom
        anchors.topMargin: 10
        height: parent.height / 2.5
        width: parent.width
        horizontalAlignment: Text.AlignHCenter
        text: "Host Game"
        color: "white"
        font.family:  fixedFont.name
        fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
    }

    Label {
        id: portLabel
        anchors.top: title.bottom
        anchors.topMargin: 10
        anchors.centerIn: parent
        anchors.horizontalCenter: parent.horizontalCenter
        horizontalAlignment: Text.AlignHCenter

        font.family:  fixedFont.name
        font.pixelSize: 72
        text: qsTr( "Port: 6969")
        color: "white"

    }

//    TextField {
//        id: portTextField
//        anchors.left: portLabel.right
//        height: portLabel.height
//        anchors.top: portLabel.top
//        font.family:  fixedFont.name
//        font.pixelSize: 72
//        style: TextFieldStyle{
//            background: Rectangle {
//                color: "#143366"
//            }
//            textColor: "white"
//        }
//        horizontalAlignment: Text.AlignHCenter
//    }

    Rectangle {
        id: startPushButton
        height: parent.height / 15
        width: parent.width / 15
        anchors.top: portLabel.bottom
        anchors.topMargin: 20
        anchors.horizontalCenter: parent.horizontalCenter
        color: "white"

        Text {
            height: parent.height
            width: parent.width
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: "Start"
            color: "#1e4c96"
            font.family:  fixedFont.name
            fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
        }

        MouseArea {
            anchors.fill: parent
            onPressed: parent.color = "grey"
            onReleased: parent.color = "white"
            onClicked: gManager.StartHosting( 6969 )
        }
    }
}
