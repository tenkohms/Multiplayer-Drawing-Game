import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import GuessPageWrapper 1.0

Item {
    anchors.fill: parent

    GuessPage {
        id: guessPage
        onSubmitGuess: {

            gManager.ReceiveGuessFromGuessPage( Guess )
            //gManager.SetNewLoaderPage( Page )
        }
        onSetNewPage: gManager.SetNewLoaderPage( Page )
    }

    Component.onCompleted: {
        gManager.onNewGuessWord.connect( guessPage.setImageLocation )
        mainWindow.hideChatWindow()
    }

    Text {
        id: label
        anchors.top: parent.top
        anchors.topMargin: 10
        anchors.horizontalCenter: parent.horizontalCenter
        font.family: fixedFont.name
        color: "white"
        text: "What is this??"
        font.pixelSize: 40 * ( parent.height / 675)
    }

    Image {
        id: picLoader
        anchors {
            top: guessTextField.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            margins: 15
        }

        fillMode: Image.PreserveAspectFit

        source: guessPage.imageLocation
    }

    TextField {
        id: guessTextField

        anchors {
            top: label.bottom
            topMargin: 15
            left: parent.left
            leftMargin: 15
        }

        width: parent.width / 1.25
        style: TextFieldStyle {
            background: Rectangle {
                color: "#1e4c96"
            }
            textColor: "white"
        }

        horizontalAlignment: Text.AlignHCenter
        onAccepted: {
            if ( guessTextField.text !== ""){
                guessPage.SubmitGuess( guessTextField.text )
            }
        }

        maximumLength: 60

        font.pixelSize: 30 * ( parent.height / 675)
        font.family: guwul.name

    }

    Rectangle {
        id: sendButton
        height: guessTextField.height + underline.height
        anchors.top: guessTextField.top
        anchors.left: guessTextField.right
        anchors.leftMargin: 5
        anchors.right: parent.right
        anchors.rightMargin: 15

        color: "#33c63f"

        Text {
            height: parent.height
            width: parent.width * .8
            anchors.centerIn: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: "Submit"
            color: "white"
            font.family:  fixedFont.name
            fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
        }

        MouseArea {
            anchors.fill: parent
            onPressed: parent.color = "#218429"
            onReleased: parent.color = "#33c63f"
            onClicked: {
                if ( guessTextField.text !== ""){
                    guessPage.SubmitGuess( guessTextField.text )
                }
            }
        }
    }

    Rectangle {
        id: underline
        anchors.top: guessTextField.bottom
        anchors.left: guessTextField.left
        width: guessTextField.width
        height: 2
        color: "white"
    }

}
