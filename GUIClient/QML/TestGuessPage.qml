import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import GuessPageWrapper 1.0

Item {
    anchors.fill: parent

    GuessPage {
        id: guessPage
        onSubmitGuess: gManager.ReceiveGuessFromGuessPage( Guess )
        onSetNewPage: gManager.SetNewLoaderPage( Page )
    }

    Component.onCompleted: {
        gManager.onNewGuessWord.connect( guessPage.setImageLocation )
    }

    Text {
        id: label
        anchors.top: parent.top
        anchors.topMargin: 10
        anchors.horizontalCenter: parent.horizontalCenter
        font.family: fixedFont.name
        color: "white"
        text: "What is this??"
        font.pixelSize: 40
    }

    Rectangle {
        id: picBG
        anchors.top: label.bottom
        anchors.topMargin: 10
        anchors.horizontalCenter: parent.horizontalCenter

        height: 465
        width: 800
        color: "white"
    }

    Image {
        id: picLoader

        anchors.centerIn: picBG
        height: 465
        width: 800

        source: guessPage.imageLocation
    }

    TextField {
        id: guessTextField
        anchors.top: picLoader.bottom
        anchors.topMargin: 15
        anchors.left: parent.left
        anchors.leftMargin: 15
        width: parent.width / 1.25
        style: TextFieldStyle {
            background: Rectangle {
                color: "#1e4c96"
            }
            textColor: "white"
        }

        font.pixelSize: 30
        font.family: guwul.name

    }

    Rectangle {
        id: sendButton
        height: guessTextField.height + underline.height
        anchors.top: guessTextField.top
        anchors.left: guessTextField.right
        anchors.leftMargin: 5
        anchors.right: parent.right
        anchors.rightMargin: 15

        color: "#33c63f"

        Text {
            height: parent.height
            width: parent.width * .8
            anchors.centerIn: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: "Submit"
            color: "white"
            font.family:  fixedFont.name
            fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
        }

        MouseArea {
            anchors.fill: parent
            onPressed: parent.color = "#218429"
            onReleased: parent.color = "#33c63f"
            onClicked: guessPage.SubmitGuess( guessField.text )
        }
    }

    Rectangle {
        id: underline
        anchors.top: guessTextField.bottom
        anchors.left: guessTextField.left
        width: guessTextField.width
        height: 2
        color: "white"
    }

}
