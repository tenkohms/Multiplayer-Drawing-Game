import QtQuick 2.0

Rectangle {

    property string buttonText;
    property string textColor;
    property string defaultColor;
    property string pressedColor;
    property bool buttonEnabled: true
    signal go()

    id: button
    color: {
        if ( enabled )
        {
            defaultColor
        }
        else
        {
            pressedColor
        }
    }

    Text {
        id: bText
        height: parent.height
        width: parent.width * .8
        anchors.centerIn: parent
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        text: buttonText
        color: textColor
        font.family:  fixedFont.name
        fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
    }

    MouseArea {
        id: mArea
        enabled: buttonEnabled
        anchors.fill: parent
        onPressed: parent.color = pressedColor
        onReleased: parent.color = defaultColor
        onClicked: button.go()
    }
}
