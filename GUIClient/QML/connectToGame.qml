import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2

import ConnectToGameWrapper 1.0
Item {
    anchors.fill: parent

    ConnectToGame {
        id: ctg
        onErrorMessageChanged: {
            errorDialog.text = error;
            errorDialog.visible = true;
        }
    }

    Component.onCompleted: {
        gManager.onJoinError.connect( ctg.setErrorMessage )
    }

    GameButton {
        id: backButton
        height: parent.height / 15
        width: parent.width / 15
        anchors.top: parent.top
        anchors.topMargin: 15
        anchors.left: parent.left
        anchors.leftMargin: 15

        defaultColor: "white"
        pressedColor: "grey"
        textColor: "#1e4c96"
        buttonText: "Back"
        onGo: gManager.SetNewLoaderPage( "HostOrJoin.qml" )
    }

    MessageDialog {
        id: errorDialog
        title: "Join Error"
        text: ""
        onAccepted: {
            joinPushButton.color = "white"
            joinPushButton.enabled = true;
        }
        Component.onCompleted: visible = false
    }

    Text {
        id: title

        anchors.top: backButton.bottom
        anchors.topMargin: 10
        height: parent.height / 2.5
        width: parent.width
        horizontalAlignment: Text.AlignHCenter
        text: "Join Game"
        color: "white"
        font.family:  fixedFont.name
        fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
    }

    TextField {
        id: roomCodeTextField
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: title.bottom
        anchors.topMargin: 20
        width: parent.width / 2
        height: parent.height / 6
        horizontalAlignment: Text.AlignHCenter
        placeholderText: "Room Code"
        style: TextFieldStyle {
            background: Rectangle {
                color: "#143366"
            }
            textColor: "white"
        }

        font.pixelSize: 30 * ( height / 112 )
        font.family: fixedFont.name
        Keys.onReturnPressed: {
            gManager.JoinUsingRoomCode( roomCodeTextField.text )
            joinPushButton.color = "grey"
            joinPushButton.enabled = false;
        }
    }



    GameButton {
        id: joinPushButton
        height: parent.height / 15
        width: roomCodeTextField.width / 2
        anchors.top: roomCodeTextField.bottom
        anchors.topMargin: 15
        anchors.horizontalCenter: parent.horizontalCenter

        defaultColor: "white"
        pressedColor: "grey"
        textColor: "#1e4c96"
        buttonText: "Join"
        onGo: {
            gManager.JoinUsingRoomCode( roomCodeTextField.text )
            joinPushButton.color = "grey"
            joinPushButton.enabled = false;
        }
    }
}
