import QtQuick 2.0
import QtQuick.Controls 1.4

Item {
    anchors.fill: parent

    Component.onCompleted: {
        mainWindow.showChatWindow()
    }

    Text{
        id: roundProgressLabel
        anchors.top: parent.top
        anchors.topMargin: 20
        anchors.horizontalCenter: parent.horizontalCenter
        color: "white"
        font.family: fixedFont.name
        height: parent.height / 15
        horizontalAlignment: Text.AlignHCenter
        fontSizeMode: Text.VerticalFit; minimumPixelSize: 10; font.pixelSize: 72
        text: "Round " + loadingPage.roundProgress + " of " + loadingPage.roundTotal
    }

    AnimatedImage {
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        height: parent.height / 4
        id: busyIndicator
        playing: true
        fillMode: Image.PreserveAspectFit
        source: "qrc:/Images/TurtleWalk.gif"
    }

    Text {
        id: waitLabel
        anchors.top: busyIndicator.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        color: "white"
        font.family: fixedFont.name
        height: parent.height / 15
        horizontalAlignment: Text.AlignHCenter
        fontSizeMode: Text.VerticalFit; minimumPixelSize: 10; font.pixelSize: 72
        text: "Waiting for all players to submit..."
    }

    Text {
        id: progressLabel
        anchors.top: waitLabel.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        color: "white"
        font.family: fixedFont.name
        height: parent.height / 15
        horizontalAlignment: Text.AlignHCenter
        fontSizeMode: Text.VerticalFit; minimumPixelSize: 10; font.pixelSize: 72
        text: loadingPage.progress + " / " + loadingPage.total + " players ready"
    }

    GameButton {
        id: resendButton
        height: parent.height / 15
        width: parent.width / 15
        anchors.top: progressLabel.bottom
        anchors.horizontalCenter: parent.horizontalCenter

        defaultColor: "red"
        pressedColor: "white"
        textColor: "white"
        buttonText: "Resend"
        onGo: gManager.ReSendQMLSlot()

    }
}
