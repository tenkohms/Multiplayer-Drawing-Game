import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.0
import QtQuick.Controls.Styles 1.4

import LobbyPageWrapper 1.0

Item {
    anchors.fill: parent
    id: lPage
    LobbyWrapper {
        id: lobbyWrapper

        onNewChatMessageReceived: {
            messageTextArea.append( clientID + ": " + message)
        }

        onReadyListChanged: {
            inlobbylist.model = tempModel
            inlobbylist.model = lobbyWrapper.clientList
        }
    }

    ListModel {
        id: tempModel
    }

    Component.onCompleted: {
        gManager.onNewClientList.connect( lobbyWrapper.setClientList )
        gManager.onNewChatMessage.connect( lobbyWrapper.receiveNewChatMessage )
        gManager.onReadyList.connect( lobbyWrapper.ReceiveReadyList )
        gManager.AskForClients()
        gManager.RequestClientList()
        mainWindow.hideChatWindow()
    }

    Label {
        id: roomLabel
        anchors.top: parent.top
        anchors.topMargin: 15
        anchors.horizontalCenter: parent.horizontalCenter
        height: parent.height / 12
        width: parent.width
        fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 72
        font.family: fixedFont.name
        color: "white"
        text: "Lobby Room Code: " + gManager.RoomCode()
        horizontalAlignment: Text.AlignHCenter
    }


    /////// chat portion

    function sendMessage() {
        if (messageTextField.text != "")
        {
            gManager.ChatMessageToSocketSlot( messageTextField.text )
            messageTextField.text = ""
        }
    }

    GameButton {
        id: messageSendButton
        height: messageTextField.height
        width: parent.width / 15

        anchors.top: messageTextField.top
        anchors.right: parent.right
        anchors.rightMargin: 15

        defaultColor: "#33c63f"
        pressedColor: "#218429"
        textColor: "#1e4c96"
        buttonText: "Send"

        onGo: sendMessage()

    }

    TextField {
        id: messageTextField
        anchors.right: messageSendButton.left
        anchors.rightMargin: 15
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 15
        anchors.left: messageTextArea.left
        maximumLength: 30
        height: parent.height / 10
        font.pixelSize: 10 * ( height / 25 )
        onAccepted: {
            sendMessage()
        }
    }

    TextArea {
        id: messageTextArea
        anchors.top: roomLabel.bottom
        anchors.topMargin: 15
        anchors.bottom: messageTextField.top
        anchors.bottomMargin: 15
        anchors.right: parent.right
        anchors.rightMargin: 15
        font.family: fixedFont.name
        font.pixelSize: 30 * ( height / 491.25 )

        style: TextAreaStyle {
            backgroundColor: "#143366"
            textColor: "white"
        }

        width: parent.width / 2 - 45
        readOnly: true
    }

    /////// lobby info portion

    Label {
        id: inlobbylabel
        anchors.left: parent.left
        anchors.leftMargin: 15
        anchors.top: roomLabel.bottom
        anchors.topMargin: 15
        font.family: fixedFont.name
        font.pixelSize: 60
        color: "white"
        text: "In Lobby: "
    }

    ListView {
        id: inlobbylist
        anchors.top:inlobbylabel.bottom
        anchors.bottom: parent.bottom
        anchors.left: inlobbylabel.left
        anchors.rightMargin: 5
        width: messageTextArea.width
        model: lobbyWrapper.clientList

        delegate: Text {
                text: modelData
                font.family: fixedFont.name
                font.pixelSize: 60
                color: "white"
                GameButton{
                    anchors.left: parent.right
                    anchors.leftMargin: 15
                    anchors.top: parent.top

                    height: parent.height * .9
                    width: lPage.width / 15

                    visible: lobbyWrapper.isReady( modelData )

                    defaultColor: "#33c63f"
                    pressedColor: "#33c63f"
                    buttonText: "Ready"
                    textColor: "white"
                    buttonEnabled: false
                }
            }
    }


    GameButton {
        id: readyButton
        width: parent.width / 15

        anchors.verticalCenter: roomLabel.verticalCenter
        anchors.right: parent.right

        anchors {
            top: parent.top
            bottom: messageTextArea.top
            topMargin: 10
            bottomMargin: 10
        }

        defaultColor: "#33c63f"
        pressedColor: "#218429"
        textColor: "#1e4c96"
        buttonText: "Ready!"

        onGo: gManager.ReadyToStart()

    }
}
