#include "messageprocessor.h"
#include <QCryptographicHash>
#include <QMessageAuthenticationCode>
#include <QDebug>

MessageProcessor::MessageProcessor(QObject *parent) :
    QObject(parent),
    testIgnore_( false )
{

}

QString MessageProcessor::CreateImageFileMessage(int roomCode, int round, QString fileData, QString artist, QString clientID)
{
    QByteArray key = "031190";
    QMessageAuthenticationCode code( QCryptographicHash::Sha1 );
    code.setKey( key );
    code.addData( fileData.toLatin1() );
    QString md5 = code.result().toHex();

    QString message = "{\"type\":\"fileTransfer\",\"checksum\":\"" + md5 + "\",\"roomCode\":" + QString::number( roomCode ) + ",\"round\":" + QString::number( round ) + ",\"data\":\"" + fileData + "\",\"sender\":\"" + clientID + "\",\"artist\":\"" + artist + "\"}";
    return message;
}

QString MessageProcessor::CreateGuess(int roomCode, int round, QString Guess, QString artist, QString clientID)
{
    QString message = "{\"type\":\"guessWord\",\"round\":" + QString::number( round ) + ",\"guess\":\"" + Guess + "\",\"sender\":\"" + clientID + "\",\"artist\":\"" + artist + "\"}";
    return message;
}

QString MessageProcessor::CreateChangeRoundMessage(int roomCode, int round)
{
    QString message = "{\"type\":\"changeRound\",\"roomCode\":" + QString::number( roomCode ) + ",\"round\":" + QString::number( round ) + "}";
    return message;
}

QString MessageProcessor::CreateVoteSelectedMessage(int roomCode, QString player)
{
    QString message = "{\"type\":\"voteSelect\",\"roomCode\":" + QString::number( roomCode ) + ",\"player\":\"" + player + "\"}";
    return message;
}

QString MessageProcessor::CreateFinalMessage(QMap<QString, int> scores)
{
    QString message = "{\"type\":\"finalScores\",\"payLoad\":[";
    foreach( const QString player, scores.keys() )
    {
        message.append( "{\"player\":\"" + player + "\",\"score\":" + QString::number( scores[ player ] ) + "}," );

    }
    message = message.mid( 0, message.size() - 1 );
    message.append( "]}");
    return message;
}

QString MessageProcessor::GetMessageType( QString jsonMessage )
{
    return QString( QJsonDocument::fromJson( jsonMessage.toLatin1() ).object().value( "type" ).toString() );
}

void MessageProcessor::ProcessMessage(QString message)
{
    qDebug() << "Message type: " << GetMessageType( message );
}

void MessageProcessor::setIgnore(bool ignore)
{
    testIgnore_ = ignore;
}
