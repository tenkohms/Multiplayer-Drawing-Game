#ifndef MESSAGEPROCESSOR_H
#define MESSAGEPROCESSOR_H

#include <QObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QMap>
#include <QPair>

class MessageProcessor : public QObject
{
    Q_OBJECT
public:
    explicit MessageProcessor(QObject *parent = nullptr);
    QString CreateImageFileMessage( int roomCode, int round, QString fileData, QString artist, QString clientID );
    QString CreateGuess( int roomCode, int round, QString Guess, QString artist, QString clientID );
    QString CreateChangeRoundMessage( int roomCode, int round );
    QString CreateVoteSelectedMessage( int roomCode, QString player );
    QString CreateFinalMessage( QMap< QString, int > scores );

signals:
    void forwardMessage( QString message );
    void voteSelect( QString Player );
    void finalScores( QMap<QString, int> );

    //all
    void chatMessage( QString message, QString clientID );
    void registerAlias( QString OldAlias, QString NewAlias );
    void aliasSetFailed( bool );
    void clientList( int, QStringList );

    //client
    void roundReady( QString Word );
    void newGuessRound( int round, QString artist, bool isImage, QString Data );
    void finalGuesses(int rounds, QString op, QString word, QMap< int, bool> roundIsImageMap, QMap< int, QPair< QString, QString> > roundPlayerGuess, QStringList displayWord );
    void guessesReceivedProgress( int, int, int, int );
    void changeRound( int round );
    void reqClientList(QString clientID);
    void joinError( QString error );
    void ipBan();
    void ReadyList( QStringList readyList );

    //host
    void DebugMessage( QString );
    void CreateRoom( QString clientID);
    void JoinRoom( int roomCode, QString ClientID );
    void ReadyToStart( int roomCode, QString sender );
    void ChangeTimelineRound( int roomCode, int round);
    void VotedForPlayer( int roomCode, QString Player );

    void imageFileData( int round, QString fileData, QString artist, QString clientID );
    void guessData( int round, QString guess, QString artist, QString clientID );

    void banPlayer( QString player );

public slots:
    virtual QString GetMessageType( QString jsonMessage );
    virtual void ProcessMessage( QString message );
    void setIgnore( bool ignore);

protected:
    bool testIgnore_;
};

#endif // MESSAGEPROCESSOR_H
