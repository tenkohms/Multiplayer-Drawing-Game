#ifndef SETALIASWRAPPER_H
#define SETALIASWRAPPER_H

#include <QObject>

class SetAliasWrapper : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool failedAliasSet READ failedAliasSet WRITE setFailedAliasSet NOTIFY failedAliasSetChanged)
public:
    explicit SetAliasWrapper(QObject *parent = nullptr);
    bool failedAliasSet();

signals:
    void failedAliasSetChanged( bool );
    void changeLoader( QString );
    void newAlias( QString alias);
    void enableButton();
    void aliasSet();

public slots:
    void setFailedAliasSet( bool FailedAliasSet );
    void setReadyForNext( bool ReadyForNext );
    void validateAndSendAlias( QString );
private:
    bool failedAliasSet_;
    bool readyForNext_;
};

#endif // SETALIASWRAPPER_H
