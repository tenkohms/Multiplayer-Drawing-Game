#ifndef SCORESCREENWRAPPER_H
#define SCORESCREENWRAPPER_H

#include <QObject>
#include <QMap>

class ScoreScreenWrapper : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString names READ names WRITE setNames NOTIFY namesChanged)

public:
    explicit ScoreScreenWrapper(QObject *parent = nullptr);
    QString names();

signals:
    void namesChanged( QString );
    void newPage ( QString Page );
public slots:
    void setNames( QString);
    void Done();

private:
    QString names_;
};

#endif // SCORESCREENWRAPPER_H
