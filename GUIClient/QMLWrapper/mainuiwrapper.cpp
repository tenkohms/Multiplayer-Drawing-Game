#include "mainuiwrapper.h"

MainUiWrapper::MainUiWrapper(QObject *parent) :
    QObject(parent),
    loaderPage_("mainMenuPage.qml")
{

}

QString MainUiWrapper::loaderPage()
{
    return loaderPage_;
}

void MainUiWrapper::setLoaderPage(QString LoaderPage)
{
    if ( loaderPage_ != LoaderPage )
    {
        loaderPage_ = LoaderPage;
        emit loaderPageChanged( loaderPage_ );
    }
}


void MainUiWrapper::receiveNewNotification(QString message, QString color)
{
    emit newNotification( message, color);
}

void MainUiWrapper::chatSlot(QString message)
{
    emit newChatSend( message );
}
