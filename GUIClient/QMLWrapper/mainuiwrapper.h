#ifndef MAINUIWRAPPER_H
#define MAINUIWRAPPER_H

#include <QObject>

class MainUiWrapper : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString loaderPage READ loaderPage WRITE setLoaderPage NOTIFY loaderPageChanged)
public:
    explicit MainUiWrapper(QObject *parent = nullptr);
    QString loaderPage();

signals:
    void loaderPageChanged( QString );
    void newNotification( QString message, QString color );
    void newChatSend( QString message );
public slots:
    void setLoaderPage( QString LoaderPage );
    void receiveNewNotification( QString message, QString color );
    void chatSlot( QString message );
private:
    QString loaderPage_;
};

#endif // MAINUIWRAPPER_H
