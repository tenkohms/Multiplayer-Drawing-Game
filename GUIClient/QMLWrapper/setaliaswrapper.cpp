#include "setaliaswrapper.h"

SetAliasWrapper::SetAliasWrapper(QObject *parent) :
    QObject(parent),
    failedAliasSet_( false ),
    readyForNext_( false )
{

}

bool SetAliasWrapper::failedAliasSet()
{
    return failedAliasSet_;
}

void SetAliasWrapper::setReadyForNext( bool ReadyForNext )
{
    readyForNext_ = ReadyForNext;
}

void SetAliasWrapper::validateAndSendAlias(QString alias)
{
    if ( alias.contains("\"") || alias.contains("\\"))
    {
        emit enableButton();
    }
    else
    {
        emit newAlias( alias );
    }
}

void SetAliasWrapper::setFailedAliasSet(bool FailedAliasSet)
{
    if ( failedAliasSet_ != FailedAliasSet )
    {
        failedAliasSet_ = FailedAliasSet;
        emit failedAliasSetChanged( failedAliasSet_ );
    }

    if ( ( FailedAliasSet == false ) && readyForNext_ )
    {
        emit aliasSet();
    }
}
