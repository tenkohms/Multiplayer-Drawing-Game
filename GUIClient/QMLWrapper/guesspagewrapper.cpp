#include "guesspagewrapper.h"

GuessPageWrapper::GuessPageWrapper(QObject *parent) :
    QObject(parent),
    imageLocation_( QString() )
{

}

QString GuessPageWrapper::imageLocation()
{
    return imageLocation_;
}

void GuessPageWrapper::setImageLocation(QString Location)
{
    if ( imageLocation_ != Location )
    {
        QString newLocation = "file:///" + Location;
        imageLocation_ = newLocation;
        emit imageLocationChanged( imageLocation_ );
    }
}

void GuessPageWrapper::SubmitGuess(QString Guess)
{
    bool safe = true;

    if ( Guess.contains( "\"") || Guess.contains( "\\"))
        safe = false;

    if ( safe ){
        emit submitGuess( Guess );
        emit setNewPage("LoadingPage.qml");
    }
}
