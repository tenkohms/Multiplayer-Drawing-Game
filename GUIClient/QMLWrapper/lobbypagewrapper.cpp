#include "lobbypagewrapper.h"
#include <QDebug>
LobbyPageWrapper::LobbyPageWrapper(QObject *parent) :
    QObject(parent),
    clientList_( QStringList() ),
    readyList_( QStringList() )
{
}

void LobbyPageWrapper::receiveNewChatMessage(QString message, QString clientID)
{
    //show new message in lobby ui
    emit newChatMessageReceived( message, clientID );
}

QStringList LobbyPageWrapper::clientList()
{
    return clientList_;
}

void LobbyPageWrapper::sendNewChatMessage(QString message)
{
    //broadcast new message we want to send via socket
    emit sendNewChatMessageSignal( message );
}

void LobbyPageWrapper::setClientList(QStringList ClientList )
{
    if ( clientList_ != ClientList )
    {
        clientList_ = ClientList;

    }
    emit clientListChanged( clientList_ );
}

void LobbyPageWrapper::ReceiveReadyList(QStringList newList)
{
    if (readyList_ != newList )
    {
        readyList_ = newList;
        emit readyListChanged();
        emit clientListChanged( clientList_);
    }
}

bool LobbyPageWrapper::isReady(QString playerName)
{
    if ( readyList_.contains( playerName ) )
    {
        return true;
    }
    else
        return false;
}
