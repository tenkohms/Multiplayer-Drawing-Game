#include "mainmenuwrapper.h"

MainMenuWrapper::MainMenuWrapper(QObject *parent) : QObject(parent)
{
}

void MainMenuWrapper::HostGameButtonSlot()
{
    emit hostGameButtonPressed();
}

void MainMenuWrapper::JoinGameButtonSlot()
{
    emit joinGameButtonPressed();
}
