#include "connecttogamewrapper.h"

ConnectToGameWrapper::ConnectToGameWrapper(QObject *parent)
    : QObject(parent),
      errorMessage_( QString() )
{

}

QString ConnectToGameWrapper::errorMessage()
{
    return errorMessage_;
}

void ConnectToGameWrapper::setErrorMessage(QString ErrorMessage )
{
    if ( errorMessage_ != ErrorMessage )
    {
        errorMessage_ = ErrorMessage;
        emit errorMessageChanged( errorMessage_);
    }
}
