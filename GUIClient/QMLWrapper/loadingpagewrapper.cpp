#include "loadingpagewrapper.h"

LoadingPageWrapper::LoadingPageWrapper(QObject *parent) :
    QObject(parent),
    progress_( 0 ),
    total_( 0 ),
    roundProgress_( 0 ),
    roundTotal_( 0 )
{

}

int LoadingPageWrapper::progress()
{
    return progress_;
}

int LoadingPageWrapper::total()
{
    return total_;
}

void LoadingPageWrapper::setTotal(int total){
    if ( total_ != total )
    {
        total_ = total;
        emit totalChanged( total_);
    }
}

void LoadingPageWrapper::setProgress(int progress)
{

    if ( progress_ != progress)
    {
        if ( progress >= 1 )
        {
            emit playSound();
        }
        progress_ = progress;
        emit progressChanged( progress_);
    }
}

int LoadingPageWrapper::roundProgress()
{
    return roundProgress_;
}

void LoadingPageWrapper::setRoundProgress( int rProgress )
{
    if ( roundProgress_ != rProgress )
    {
        roundProgress_ = rProgress;
        emit roundProgressChanged( roundProgress_);
    }
}

int LoadingPageWrapper::roundTotal()
{
    return roundTotal_;
}

void LoadingPageWrapper::setRoundTotal(int roundTotal )
{
    if ( roundTotal_ != roundTotal )
    {
        roundTotal_ = roundTotal;
        emit roundTotalChanged( roundTotal_);
    }
}
