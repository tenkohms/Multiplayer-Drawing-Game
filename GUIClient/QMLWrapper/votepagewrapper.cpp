#include "votepagewrapper.h"

VotePageWrapper::VotePageWrapper(QObject *parent) :
    QObject(parent),
    origWord_( QString() ),
    guessData_( QString() ),
    currentPlayer_( QString() ),
    displayWord_( QString() ),
    isImage_( true ),
    rounds_( 0 ),
    currentRound_( 0 )
{

}

int VotePageWrapper::currentRound()
{
    return currentRound_;
}

void VotePageWrapper::setCurrentRound(int round )
{
    if ( currentRound_ != round )
    {
        currentRound_ = round;
        emit currentRoundChanged( currentRound_);
    }
}

QString VotePageWrapper::origWord()
{
    return origWord_;
}

void VotePageWrapper::setOrigWord(QString Word)
{
    if ( origWord_ != Word )
    {
        origWord_ = Word;
        emit origWordChanged( origWord_ );
    }
}

void VotePageWrapper::GetGuess(int Round)
{
    emit requestGuess( Round );
}

bool VotePageWrapper::isImage()
{
    return isImage_;
}

void VotePageWrapper::setIsImage( bool isImage )
{
    if ( isImage_ != isImage )
    {
        isImage_ = isImage;
        emit isImageChanged( isImage_ );
    }

}

QString VotePageWrapper::guessData()
{
    return guessData_;
}

void VotePageWrapper::setGuessData( QString data )
{
    if ( guessData_ != data )
    {
        guessData_ = data;
    }
    emit guessDataChanged( guessData_ );
}

int VotePageWrapper::rounds()
{
    return rounds_;
}

void VotePageWrapper::setRounds(int Rounds )
{
    rounds_ = Rounds;

    for ( int i = 0; i < rounds_ - 1; i++ )
    {
        GetGuess( i );
    }

    GetGuess( 0 );
    emit roundsChanged( rounds_ );
}

QString VotePageWrapper::currentPlayer()
{
    return currentPlayer_;

}

void VotePageWrapper::setCurrentPlayer(QString Player)
{
    if ( currentPlayer_ != Player )
    {
        currentPlayer_ = Player;
        emit currentPlayerChanged( currentPlayer_);
    }
}

void VotePageWrapper::changeRoundSlot(int inc)
{

    if ( ( inc == 1 ) && ( currentRound_ < rounds_ - 1) )
    {
        setCurrentRound( currentRound_ + 1);
        GetGuess( currentRound_ );
        emit changeRound( currentRound_ );
    }

    if ( ( inc == -1 ) && ( currentRound_ > 0 ) )
    {
        setCurrentRound( currentRound_ - 1);
        GetGuess( currentRound_ );
        emit changeRound( currentRound_ );
    }

}

void VotePageWrapper::voteSelectSlot()
{
    emit voteSelect( currentPlayer_ );
}

QString VotePageWrapper::displayWord()
{
    return displayWord_;
}

void VotePageWrapper::setDisplayWord(QString word )
{
    if ( displayWord_ != word )
    {
        displayWord_ = word;
        emit displayWordChanged( displayWord_ );
    }
}
