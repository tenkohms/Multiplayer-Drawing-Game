#ifndef GUESSPAGEWRAPPER_H
#define GUESSPAGEWRAPPER_H

#include <QObject>
#include <QStandardPaths>

class GuessPageWrapper : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString imageLocation READ imageLocation WRITE setImageLocation NOTIFY imageLocationChanged)

public:
    explicit GuessPageWrapper(QObject *parent = nullptr);
    QString imageLocation();
signals:
    void imageLocationChanged( QString );
    void submitGuess( QString Guess );
    void setNewPage( QString Page );

public slots:
    void setImageLocation( QString );
    void SubmitGuess( QString );

private:
    QString imageLocation_;

};

#endif // GUESSPAGEWRAPPER_H
