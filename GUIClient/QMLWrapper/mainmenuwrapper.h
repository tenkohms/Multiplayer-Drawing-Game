#ifndef MAINMENUWRAPPER_H
#define MAINMENUWRAPPER_H

#include <QObject>

class MainMenuWrapper : public QObject
{
    Q_OBJECT
public:
    explicit MainMenuWrapper(QObject *parent = nullptr);

signals:
    void hostGameButtonPressed();
    void joinGameButtonPressed();

public slots:
    void HostGameButtonSlot();
    void JoinGameButtonSlot();
};

#endif // MAINMENUWRAPPER_H
