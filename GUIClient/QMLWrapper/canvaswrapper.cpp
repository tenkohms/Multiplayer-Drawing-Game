#include "canvaswrapper.h"
#include <QStandardPaths>
#include <QFile>
#include <QList>
#include <QDebug>

CanvasWrapper::CanvasWrapper(QObject *parent) :
    QObject(parent),
    roundWord_( QString() ),
    roundTimerProgress_( 0 )
{
    setImageFileLocation( QString( QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory) ) );

}

QString CanvasWrapper::roundWord()
{
    return roundWord_;
}

void CanvasWrapper::setRoundWord( QString Word )
{
    if ( roundWord_ != Word )
    {
        roundWord_ = Word;
        emit roundWordChanged( roundWord_ );
    }
}

double CanvasWrapper::roundTimerProgress()
{
    return roundTimerProgress_;
}

void CanvasWrapper::setRoundTimerProgress(double val )
{
    if ( roundTimerProgress_ != val )
    {
        roundTimerProgress_ = val;
        emit roundTimerProgressChanged( val );
    }
}

QString CanvasWrapper::imageFileLocation()
{
    return imageFileLocation_;
}

void CanvasWrapper::setImageFileLocation(QString NewLocation)
{
    if ( imageFileLocation_ != NewLocation )
    {
        imageFileLocation_ = NewLocation;
        emit imageFileLocationChanged( imageFileLocation_ );
    }
}

void CanvasWrapper::setUI(QString Page)
{
    emit setUIPage( Page );
}

QString CanvasWrapper::translatePath(QString path)
{
#ifdef Q_OS_WIN
    path = path.replace( "file:///", "");

#else
    path = path.replace("file://", "");
#endif

    return path;
}

void CanvasWrapper::DrawingTimeOver()
{
    QFile imageFile( imageFileLocation() + "/imageFile.png");
    if ( imageFile.open( QIODevice::ReadWrite ) )
    {
        QString fileContents = QByteArray( imageFile.readAll() ).toHex() ;
        imageFile.close();
        imageFile.remove();

        emit imageFileData( fileContents );
    }
}

void CanvasWrapper::GetData(QVariant data)
{
    QList< QVariant > someting = data.toList();

    qDebug() << someting.size();
}
