#ifndef VOTEPAGEWRAPPER_H
#define VOTEPAGEWRAPPER_H

#include <QObject>

class VotePageWrapper : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString origWord READ origWord WRITE setOrigWord NOTIFY origWordChanged)
    Q_PROPERTY(bool isImage READ isImage WRITE setIsImage NOTIFY isImageChanged)
    Q_PROPERTY(QString guessData READ guessData WRITE setGuessData NOTIFY guessDataChanged)
    Q_PROPERTY(int rounds READ rounds WRITE setRounds NOTIFY roundsChanged)
    Q_PROPERTY(QString currentPlayer READ currentPlayer WRITE setCurrentPlayer NOTIFY currentPlayerChanged)
    Q_PROPERTY(QString displayWord READ displayWord WRITE setDisplayWord NOTIFY displayWordChanged)
    Q_PROPERTY(int currentRound READ currentRound WRITE setCurrentRound NOTIFY currentRoundChanged)


public:
    explicit VotePageWrapper(QObject *parent = nullptr);
    QString origWord();
    bool isImage();
    QString guessData();
    int rounds();
    QString currentPlayer();
    QString displayWord();
    int currentRound();

signals:
    void origWordChanged( QString );
    void requestGuess( int Round );
    void isImageChanged( bool isImage);
    void guessDataChanged( QString data);
    void roundsChanged( int Rounds );
    void currentPlayerChanged( QString );
    void changeRound( int round );
    void voteSelect( QString player );
    void displayWordChanged( QString);
    void currentRoundChanged( int round );

public slots:
    void setOrigWord( QString );
    void GetGuess( int Round );
    void setIsImage( bool );
    void setGuessData( QString );
    void setRounds( int );
    void setCurrentPlayer(QString);
    void changeRoundSlot( int );
    void voteSelectSlot( );
    void setDisplayWord( QString );
    void setCurrentRound( int );

private:
    QString origWord_, guessData_, currentPlayer_, displayWord_;
    bool isImage_;
    int rounds_, currentRound_;
};

#endif // VOTEPAGEWRAPPER_H
