#include "scorescreenwrapper.h"

ScoreScreenWrapper::ScoreScreenWrapper(QObject *parent) :
    QObject(parent)
{
    names_ = QString();
}

QString ScoreScreenWrapper::names()
{
    return names_;
}

void ScoreScreenWrapper::setNames(QString Names)
{
    if ( names_ != Names )
    {
        names_ = Names;
        emit namesChanged( names_ );
    }
}

void ScoreScreenWrapper::Done() {
    emit newPage( "lobbyPage.qml" );
}
