#ifndef LOBBYPAGEWRAPPER_H
#define LOBBYPAGEWRAPPER_H

#include <QObject>

class LobbyPageWrapper : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QStringList clientList READ clientList WRITE setClientList NOTIFY clientListChanged)

public:
    explicit LobbyPageWrapper(QObject *parent = nullptr);
    QStringList clientList();

signals:
    void newChatMessageReceived( QString message, QString clientID );
    void sendNewChatMessageSignal( QString message );
    void clientListChanged( QStringList );
    void readyListChanged();

public slots:
    void receiveNewChatMessage( QString message, QString clientID );
    void sendNewChatMessage ( QString message );
    void setClientList( QStringList );

    void ReceiveReadyList( QStringList );
    bool isReady( QString playerName );

private:
    QStringList clientList_, readyList_;
};

#endif // LOBBYPAGEWRAPPER_H
