#ifndef SERVERHOSTWRAPPER_H
#define SERVERHOSTWRAPPER_H

#include <QObject>

class ServerHostWrapper : public QObject
{
    Q_OBJECT
public:
    explicit ServerHostWrapper(QObject *parent = nullptr);

signals:
    void startServerButtonPressedSignal( QString Port );

public slots:
    void StartServerButtonPressed( QString Port );
};

#endif // SERVERHOSTWRAPPER_H
