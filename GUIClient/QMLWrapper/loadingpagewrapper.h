#ifndef LOADINGPAGEWRAPPER_H
#define LOADINGPAGEWRAPPER_H

#include <QObject>

class LoadingPageWrapper : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int progress READ progress WRITE setProgress NOTIFY progressChanged)
    Q_PROPERTY(int total READ total WRITE setTotal NOTIFY totalChanged)
    Q_PROPERTY(int roundProgress READ roundProgress WRITE setRoundProgress NOTIFY roundProgressChanged)
    Q_PROPERTY(int roundTotal READ roundTotal WRITE setRoundTotal NOTIFY roundTotalChanged)

public:
    explicit LoadingPageWrapper(QObject *parent = nullptr);
    int progress();
    int total();
    int roundProgress();
    int roundTotal();
signals:
    void progressChanged( int );
    void totalChanged( int );
    void roundProgressChanged( int );
    void roundTotalChanged( int );
    void playSound();
public slots:
    void setProgress( int );
    void setTotal( int );
    void setRoundProgress( int );
    void setRoundTotal( int );
private:
    int progress_, total_, roundProgress_, roundTotal_;

};

#endif // LOADINGPAGEWRAPPER_H
