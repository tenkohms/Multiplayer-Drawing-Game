#ifndef CANVASWRAPPER_H
#define CANVASWRAPPER_H

#include <QObject>
#include <QVariant>

class CanvasWrapper : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString roundWord READ roundWord WRITE setRoundWord NOTIFY roundWordChanged)
    Q_PROPERTY(double roundTimerProgress READ roundTimerProgress WRITE setRoundTimerProgress NOTIFY roundTimerProgressChanged)
    Q_PROPERTY(QString imageFileLocation READ imageFileLocation WRITE setImageFileLocation NOTIFY imageFileLocationChanged)
public:
    explicit CanvasWrapper(QObject *parent = nullptr);
    QString roundWord();
    double roundTimerProgress();
    QString imageFileLocation();

signals:
    void roundWordChanged( QString );
    void roundTimerProgressChanged( double );
    void imageFileLocationChanged( QString );
    void imageFileData( QString fileData );
    void setUIPage( QString Page);

public slots:
    void setRoundWord( QString );
    void setRoundTimerProgress( double );
    void DrawingTimeOver();
    void setImageFileLocation( QString );
    void setUI( QString Page );

    QString translatePath( QString path );
    void GetData( QVariant data );

private:
    QString roundWord_;
    double roundTimerProgress_;
    QString imageFileLocation_;
};

#endif // CANVASWRAPPER_H
