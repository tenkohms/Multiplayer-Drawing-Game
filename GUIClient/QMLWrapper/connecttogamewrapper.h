#ifndef CONNECTTOGAMEWRAPPER_H
#define CONNECTTOGAMEWRAPPER_H

#include <QObject>

class ConnectToGameWrapper : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString errorMessage READ errorMessage WRITE setErrorMessage NOTIFY errorMessageChanged)
public:
    explicit ConnectToGameWrapper(QObject *parent = nullptr);
    QString errorMessage();
signals:
    void errorMessageChanged( QString error);
public slots:
    void setErrorMessage( QString );

private:
    QString errorMessage_;
};

#endif // CONNECTTOGAMEWRAPPER_H
