#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQml>

#include "GameManager/gamemanager.h"

#include "QMLWrapper/mainuiwrapper.h"
#include "QMLWrapper/setaliaswrapper.h"
#include "QMLWrapper/lobbypagewrapper.h"
#include "QMLWrapper/canvaswrapper.h"
#include "QMLWrapper/guesspagewrapper.h"
#include "QMLWrapper/votepagewrapper.h"
#include "QMLWrapper/scorescreenwrapper.h"
#include "QMLWrapper/loadingpagewrapper.h"
#include "QMLWrapper/connecttogamewrapper.h"

#include "PaintBucketHandler/paintbuckethandler.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    qmlRegisterType< GameManager > ( "GameManager", 1, 0, "GManager" );
    qmlRegisterType< MainUiWrapper > ( "MainUiWrapper", 1, 0, "MUIWrapper" );
    qmlRegisterType< SetAliasWrapper > ( "SetAliasWrapper", 1, 0, "SAWrapper");
    qmlRegisterType< LobbyPageWrapper > ( "LobbyPageWrapper", 1, 0, "LobbyWrapper");
    qmlRegisterType< CanvasWrapper > ( "CanvasPageWrapper", 1, 0, "CanvasWrapper" );
    qmlRegisterType< GuessPageWrapper > ("GuessPageWrapper", 1, 0, "GuessPage" );
    qmlRegisterType< VotePageWrapper > ( "VotePageWrapper", 1, 0, "VotePage" );
    qmlRegisterType< ScoreScreenWrapper> ( "ScoreScreenWrapper", 1, 0, "ScoreScreen");
    qmlRegisterType< LoadingPageWrapper> ( "LoadingPageWrapper", 1, 0, "LoadingPage");
    qmlRegisterType< ConnectToGameWrapper >( "ConnectToGameWrapper", 1, 0, "ConnectToGame" );
    qmlRegisterType< PaintBucketHandler >( "PaintBucketHandler", 1, 0, "PaintBucket" );

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/QML/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
