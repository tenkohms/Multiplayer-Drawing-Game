#include "gameclientcontroller.h"
#include <QImage>
#include <QPainter>
#include <QBrush>
#include <QDebug>
#include <QFont>
#include <QFontDatabase>

GameClientController::GameClientController(QObject *parent)
    : QObject(parent),
      timelineImages_( QStringList() )
{

}

void GameClientController::RemoveTimelines()
{
    foreach( const QString fileName, timelineImages_ )
    {
        QFile( fileName ).remove();
    }

    timelineImages_.clear();
}

QStringList GameClientController::GetTimelines()
{
    return timelineImages_;
}

void GameClientController::StartRound(QString word)
{
    emit setNewPage( "canvasPage.qml" );
    emit newRoundWord( word );
}

void GameClientController::ReceiveGuessClient(int round, QString artist, bool isImage, QString Data)
{
    if ( !isImage )
    {
        emit setNewPage( "canvasPage.qml");
        emit newRoundWord( Data );
    }
    else
    {
        QString fileName = QString( QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory) ) + "imageFile.png";
        int fileAdder = 1;
        while ( QFile( fileName).exists() )
        {
            fileName = QString( QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory) ) + "_" + QString::number( fileAdder ) + "imageFile.png";
            fileAdder++;
        }
        QFile imageFile( fileName );
        imageFile.remove();
        if ( imageFile.open(QIODevice::ReadWrite))
        {
            imageFile.write( QByteArray::fromHex( Data.toLatin1() ) );
            imageFile.flush();
            imageFile.close();

            emit setNewPage( "GuessPage.qml" );
            emit newRoundGuess( fileName );
        }
    }

    emit currentRound( round );
    emit artistName( artist );
}

void GameClientController::ReceiveFinalGuesses(int rounds, QString op, QString word, QMap<int, bool> roundIsImageMap, QMap<int, QPair<QString, QString> > roundPlayerGuess, QStringList displayWord )
{
    fgIsImage_ = roundIsImageMap;
    roundPlayerGuess_ = roundPlayerGuess;
    displayWords_ = displayWord;

    CreateTimeline();

    emit fgRounds( rounds );
}

void GameClientController::FinalGuessRoundRequest(int round)
{
    emit fgIsImage( fgIsImage_[ round ] );
    emit fgCurrentPlayer( roundPlayerGuess_[ round ].first );
    emit fgDisplayWord( displayWords_[ round ] );

    QString fileName = QString( QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory) ) + roundPlayerGuess_[ round ].first + "-" + QString::number( round ) + ".png";
    int fileAdder = 1;

    while ( QFile( fileName ).exists() )
    {
        fileName = QString( QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory) ) + roundPlayerGuess_[ round ].first + "-" + QString::number( round ) + "-" + QString::number( fileAdder ) + ".png";
        fileAdder++;
    }
    if ( fgIsImage_[ round ] )
    {
        bool isOpen = false;
        int adder = 0;
        while ( !isOpen )
        {
            adder++;
            QFile newFile( fileName);
            isOpen = newFile.open(QIODevice::ReadWrite);
            newFile.write( QByteArray::fromHex( roundPlayerGuess_[ round ].second.toLatin1() ) );
            newFile.flush();
            newFile.close();
        }
        QString fileLocation = "file:///" + fileName;
        emit fgGuess( fileLocation );
    }
    else
        emit fgGuess( roundPlayerGuess_[ round ].second );
}

void GameClientController::CreateTimeline()
{
    QString tempTimelineDir = "ScribbleTimelines";
    //QString tempTimelineDir = QString( QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory) ) + "ScribbleTimelines";
    if ( !QDir( tempTimelineDir ).exists() )
    {
        QDir().mkdir( tempTimelineDir );
    }

    int adder = 1;

    QString fileName = tempTimelineDir + QDir::separator() + "Image" + QString::number( adder ) + ".png";
    while ( QFile( fileName ).exists() )
    {
        adder++;
        fileName = tempTimelineDir + QDir::separator() + "Image" + QString::number( adder ) + ".png";
    }

    QString tempImageFileName = QString( QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory) ) + QDir::separator() + "temp" + QString::number( adder ) + ".png";
    while ( QFile( fileName).exists() )
    {
        adder++;
        tempImageFileName = QString( QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory) ) + QDir::separator() + "temp" + QString::number( adder ) + ".png";
    }

    QFile tempImageFile( tempImageFileName );
    if ( !tempImageFile.open( QIODevice::ReadWrite ) )
    {
        return;
    }

    tempImageFile.write( QByteArray::fromHex( roundPlayerGuess_[ 1 ].second.toLatin1() ) );
    tempImageFile.flush();
    tempImageFile.close();

    QImage tempImage;
    tempImage.load( tempImageFileName );
    int imageWidth = tempImage.width();
    int imageHeight = tempImage.height();

    QImage * finalImage = new QImage( tempImage.width() * roundPlayerGuess_.size(), tempImage.height(), QImage::Format_RGB32);

    QPainter painter( finalImage );

    for ( int i = 0; i < roundPlayerGuess_.size(); i++ )
    {
        if ( fgIsImage_[ i ] )
        {
            tempImageFile.open( QIODevice::ReadWrite );
            tempImageFile.write( QByteArray::fromHex( roundPlayerGuess_[ i ].second.toLatin1() ) );
            tempImageFile.flush();
            tempImageFile.close();

            tempImage.load( tempImageFileName );

            painter.drawImage( imageWidth * i, 0, tempImage );
        }
        else
        {
            QString guessText = roundPlayerGuess_[ i ].second;
            painter.setPen( Qt::white );
            QFont f = painter.font();

            int id = QFontDatabase::addApplicationFont( ":/Font/guwul.ttf" );
            QString family = QFontDatabase::applicationFontFamilies( id ).at( 0 );
            f.setFamily( family );
            f.setPixelSize( 36 );

            painter.setFont( f );
            painter.drawText( (imageWidth * i) + ( imageWidth / 4 ), imageHeight / 2, guessText);

        }
    }

    painter.end();
    finalImage->save( fileName );

    timelineImages_.append( fileName );

    delete finalImage;
}

