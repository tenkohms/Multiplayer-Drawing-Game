#ifndef GAMECLIENTCONTROLLER_H
#define GAMECLIENTCONTROLLER_H

#include <QObject>
#include <QMap>
#include <QPair>
#include <QFile>
#include <QDir>
#include <QStandardPaths>

class GameClientController : public QObject
{
    Q_OBJECT
public:
    explicit GameClientController(QObject *parent = nullptr);

    void RemoveTimelines();
    QStringList GetTimelines();

signals:
    void setNewPage( QString Page );
    void newRoundWord( QString Data );
    void newRoundGuess( QString fileLocation );

    void currentRound( int round );
    void artistName( QString artist );

    void fgRounds( int rounds );
    void fgIsImage( bool isImage );
    void fgCurrentPlayer( QString player );
    void fgDisplayWord( QString DisplayWord );
    void fgGuess( QString data );

public slots:
    void StartRound( QString word );

    void FinalGuessRoundRequest( int round );
    void ReceiveFinalGuesses(int rounds, QString op, QString word, QMap< int, bool> roundIsImageMap, QMap< int, QPair< QString, QString> > roundPlayerGuess, QStringList displayWord );
    void ReceiveGuessClient( int round, QString artist, bool isImage, QString Data);

private:
    void CreateTimeline();

    QMap< int, bool > fgIsImage_;
    QMap< int, QPair< QString, QString >> roundPlayerGuess_;
    QStringList displayWords_;
    QStringList timelineImages_;

};

#endif // GAMECLIENTCONTROLLER_H
