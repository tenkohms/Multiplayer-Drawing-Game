TARGET = Scribble
QT += qml quick websockets
CONFIG += c++11
SOURCES += main.cpp \
    QMLWrapper/lobbypagewrapper.cpp \
    QMLWrapper/mainmenuwrapper.cpp \
    QMLWrapper/mainuiwrapper.cpp \
    QMLWrapper/setaliaswrapper.cpp \
    QMLWrapper/canvaswrapper.cpp \
    QMLWrapper/guesspagewrapper.cpp \
    QMLWrapper/votepagewrapper.cpp \
    QMLWrapper/scorescreenwrapper.cpp \
    QMLWrapper/loadingpagewrapper.cpp \
    QMLWrapper/serverhostwrapper.cpp \ 
    QMLWrapper/connecttogamewrapper.cpp \
    PaintBucketHandler/paintbuckethandler.cpp \
    GameClientController/gameclientcontroller.cpp \
    GameSettings/gamesettings.cpp \
    GameManager/gamemanager.cpp \
    SocketObject/gameclientsocket.cpp \
    MessageProcessor/clientmessageprocessor.cpp \
    SocketObject/socketobjectbase.cpp \
    MessageProcessor/messageprocessor.cpp

win32:RC_ICONS = taylor_1Dt_icon.ico

HEADERS += \
    QMLWrapper/lobbypagewrapper.h \
    QMLWrapper/mainmenuwrapper.h \
    QMLWrapper/mainuiwrapper.h \
    QMLWrapper/setaliaswrapper.h \
    QMLWrapper/canvaswrapper.h \
    QMLWrapper/guesspagewrapper.h \
    QMLWrapper/votepagewrapper.h \
    QMLWrapper/scorescreenwrapper.h \
    QMLWrapper/loadingpagewrapper.h \
    QMLWrapper/serverhostwrapper.h \ 
    QMLWrapper/connecttogamewrapper.h \
    PaintBucketHandler/paintbuckethandler.h \
    GameManager/gamemanager.h \
    GameClientController/gameclientcontroller.h \
    GameSettings/gamesettings.h \
    SocketObject/gameclientsocket.h \
    MessageProcessor/clientmessageprocessor.h \
    SocketObject/socketobjectbase.h \
    MessageProcessor/messageprocessor.h

RESOURCES += qml.qrc

DISTFILES += \
    Music/Quirky Wacky.mp3 \
    android/AndroidManifest.xml \
    android/AndroidManifest.xml \
    android/AndroidManifest.xml \
    android/AndroidManifest.xml \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/build.gradle \
    android/build.gradle \
    android/build.gradle \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew \
    android/gradlew \
    android/gradlew \
    android/gradlew \
    android/gradlew.bat \
    android/gradlew.bat \
    android/gradlew.bat \
    android/gradlew.bat \
    android/gradlew.bat \
    android/res/values/libs.xml \
    android/res/values/libs.xml \
    android/res/values/libs.xml \
    android/res/values/libs.xml \
    android/res/values/libs.xml

INCLUDEPATH += ../Common
#unix:LIBS += -L../Common -lCommon
#win32:LIBS += -LCommon

ANDROID_ABIS = armeabi-v7a

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
