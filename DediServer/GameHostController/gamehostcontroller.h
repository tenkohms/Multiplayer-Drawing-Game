#ifndef GAMEHOSTCONTROLLER_H
#define GAMEHOSTCONTROLLER_H

#include <QObject>
#include <QMap>
#include <QPair>
#include <QMutex>

#include "ImageGuessHandler/imageguesshandler.h"

class GameHostController : public QObject
{
    Q_OBJECT
public:
    GameHostController(QObject *parent , int Code , QStringList WordList);
    ~GameHostController();

    void StartGame( );
    bool IsGameStarted();

    void AddPlayer( QString clientID );
    void RemovePlayer( QString clientID_);
    void ClearGuesses();

    void SendMessageToAll( QString message );
    void ReadyToStart( QString sender );
    void SendReadyList();

signals:
    void DebugAndLog( QString Data );

    void sendMessageToPlayer( QString, QString );
    void gameDone( int roomCode );
    void newGuess( int roomCode, bool isImage, QString payLoad );
    void emptyGame(int roomCode );

public slots:
    void NextFGRound();

    //Forwards
    void ChatMessage( QString message, QString clientID );
    void ChangeTimelineRound( int round );
    void VotedForPlayer( QString Player );

    void ReceiveGuess(int round, bool isImage, QString Guess, QString artist,  QString clientID );
    void AddPoint( QString Player );
    void RequestClientList( QString Player );
    void GameFinished();

private:
    void BeginRound();
    void ProcessImages();
    void SendOutGuesses();
    void SendClientsGameProgress( int progress, int total );

    bool gameStarted_;
    int guessRound_, guessesReceived_;
    QStringList playerList_, wordList_, usedList_;
    QStringList haveGuessesFor_;
    QStringList readyMap_;
    QMap< QString, QString > playerWordMap_ ;
    QMap< QPair< QString, int >,ImageGuessHandler * > imageGuessMap_;
    QMap< QString, int> playerPointMap_;

    QMutex guessMutex_;

    int roomCode_;

    int fileNumber_;
};

#endif // GAMEHOSTCONTROLLER_H
