#include "gamehostcontroller.h"
#include <QtGlobal>
#include <QStandardPaths>
#include <QFile>
#include <QTime>
#include <QDir>
#include <QDebug>
#include <random>
#include <algorithm>

#include <QCryptographicHash>
#include <QMessageAuthenticationCode>
#include <QDebug>

GameHostController::GameHostController(QObject *parent, int Code, QStringList WordList ) :
    QObject(parent),
    gameStarted_( false ),
    guessRound_( 0 ),
    guessesReceived_( 0 ),
    playerList_( QStringList() ),
    wordList_( WordList ),
    usedList_( QStringList() ),
    roomCode_( Code ),
    fileNumber_( 0 )
{
    playerWordMap_.clear();
    playerPointMap_.clear();
}

GameHostController::~GameHostController()
{
    ClearGuesses();
}

void GameHostController::AddPlayer(QString clientID)
{
    if ( !gameStarted_ )
    {
        readyMap_.clear();
        SendReadyList();
        playerList_.append(clientID);
        QString message = "{\"type\":\"clientList\",\"roomCode\":" + QString::number( roomCode_ ) + ",\"clients\":[";
        foreach ( const QString player, playerList_ )
        {
            message.append("\"" + player + "\",");
        }

        message = message.mid( 0, message.size() - 1 );
        message.append( "]}");

        SendMessageToAll( message );
        ChatMessage( clientID + " - Joined!", "Game" );
    }
}

void GameHostController::RemovePlayer(QString clientID )
{
    guessMutex_.lock();
    DebugAndLog("GameHostController::RemovePlayer - " + clientID);
    DebugAndLog("Current Players: ");

    foreach( const QString player, playerList_ )
        DebugAndLog( player );

    playerList_.removeOne( clientID );
    DebugAndLog("Removed");

    guessMutex_.unlock();

    if ( playerList_.isEmpty() )
    {
        emit emptyGame( roomCode_ );
        return;
    }


    if ( gameStarted_ )
    {
        if ( guessesReceived_ >= playerList_.size() )
        {
            guessesReceived_ = 0;
            haveGuessesFor_.clear();
            guessRound_++;
            ProcessImages();
        }
        SendClientsGameProgress( guessesReceived_, playerList_.size() );
    }

    if ( !gameStarted_ )
    {
        readyMap_.clear();
        QString message = "{\"type\":\"clientList\",\"roomCode\":" + QString::number( roomCode_ ) + ",\"clients\":[";
        foreach ( const QString player, playerList_ )
        {
            message.append("\"" + player + "\",");
        }

        message = message.mid( 0, message.size() - 1 );
        message.append( "]}");

        if ( playerList_.isEmpty() )
        {
            emit emptyGame( roomCode_ );
        }
        else
        {
            SendMessageToAll( message );
            ChatMessage( clientID + " - Left!", "Game" );
            SendReadyList();
        }
    }
}

void GameHostController::ChatMessage(QString message, QString clientID)
{
    QByteArray key = "031190";
    QMessageAuthenticationCode code( QCryptographicHash::Sha1 );
    code.setKey( key );
    code.addData( message.toLatin1() );
    QString hash = code.result().toHex();

    QString packet( "{\"type\":\"chatMessage\",\"checksum\":\"" + hash + "\",\"message\":\"" + message +
                         "\",\"sender\":\"" + clientID + "\"}");

    SendMessageToAll( packet );
}

void GameHostController::ChangeTimelineRound(int round)
{
    QString message = "{\"type\":\"changeRound\",\"round\":" + QString::number( round ) + "}";
    SendMessageToAll( message );
}

void GameHostController::VotedForPlayer(QString Player)
{
    QString message = "{\"type\":\"voteSelect\",\"player\":\"" + Player + "\"}";
    SendMessageToAll( message );

    AddPoint( Player );

    NextFGRound();
}

void GameHostController::ClearGuesses()
{
    for ( int round = 0; round < guessRound_; round++ )
    {
        for ( int player = 0; player < playerList_.size(); player++ )
        {
            ImageGuessHandler * old = imageGuessMap_[ QPair< QString, int >( playerList_[ player ], round) ];
            delete old;
        }
    }

    readyMap_.clear();

    imageGuessMap_.clear();
    playerWordMap_.clear();
    playerPointMap_.clear();
    guessesReceived_ = 0;
    guessRound_ = 0;
    haveGuessesFor_.clear();

}

void GameHostController::SendMessageToAll(QString message)
{
    DebugAndLog( QString::number(roomCode_) + " - GameHostController::SendMessageToAll: " + message );
    foreach ( const QString player, playerList_ )
    {
        emit sendMessageToPlayer( message, player );
    }
}

void GameHostController::ReadyToStart(QString sender)
{
    if ( readyMap_.size() < 3 )
    {
        QByteArray key = "031190";
        QMessageAuthenticationCode code( QCryptographicHash::Sha1 );
        code.setKey( key );
        QString message;
        message = "Cannot ready; need at least 3 players to start!";
        code.addData( message.toLatin1() );
        QString hash = code.result().toHex();

        QString packet( "{\"type\":\"chatMessage\",\"checksum\":\"" + hash + "\",\"message\":\"" + message +
                             "\",\"sender\":\"" + "Server Message" + "\"}");

        SendMessageToAll( packet );
        return;
    }

    if ( !readyMap_.contains( sender ) )
    {
        readyMap_.append( sender );
        if ( readyMap_.size() == playerList_.size() )
        {
            StartGame();
        }
        else
        {
            SendReadyList();
        }
    }
}

void GameHostController::SendReadyList()
{
    QString message = "{\"type\":\"readyList\",\"ready\":[";
    if ( !readyMap_.isEmpty())
    {
        foreach ( const QString player, readyMap_ )
        {
            message.append( "\"" + player + "\",");
        }

        message = message.mid( 0, message.size() - 1 );
    }
    message.append("]}");

    SendMessageToAll( message );
}

void GameHostController::StartGame()
{
    gameStarted_ = true;

    for ( int player = 0; player < playerList_.size(); player++ )
    {
        playerPointMap_[ playerList_[ player ] ] = 0;
    }

    QTime time = QTime::currentTime();

    std::srand((uint)time.msec());
    std::random_shuffle( playerList_.begin(), playerList_.end() );
    BeginRound();
}

bool GameHostController::IsGameStarted()
{
    return gameStarted_;
}


void GameHostController::BeginRound()
{
    playerWordMap_.clear();

    QTime time = QTime::currentTime();
    qsrand( (uint)time.msec());


    guessRound_ = 0;
    for ( int player = 0; player < playerList_.size(); player++ )
    {
        if ( usedList_.size() == wordList_.size() )
        {
            usedList_.clear();
        }

        int selector;
        do {
            selector = qrand() % ((wordList_.size() ) - 0) + 0;
        } while( usedList_.contains( wordList_[ selector ] ) );

        usedList_.append( wordList_[ selector ] );

        QString message = "{\"type\":\"roundReady\",\"word\":\"" + QString( wordList_[ selector ]).replace("'", "\'") + "\"}";

        playerWordMap_[ playerList_[ player ] ] = wordList_[ selector ];
        emit sendMessageToPlayer( message, playerList_[ player ] );
    }
    SendClientsGameProgress( guessesReceived_, playerList_.size() );
}

void GameHostController::ProcessImages()
{
    if ( guessRound_ < playerList_.size() )
    {
        for ( int i = 0; i < playerList_.size(); i++ )
        {
            int index = i + guessRound_;
            if ( index >= playerList_.size() )
            {
                index = i + guessRound_ - playerList_.size();
            }

            int gNumber = guessRound_ - 1;

            ImageGuessHandler * old = imageGuessMap_[ QPair< QString, int >( playerList_[ index ], gNumber) ];
            QString message = "{\"type\":\"newGuessRound\",\"op\":\"" + playerList_[ index ] + "\",\"round\":" + QString::number( guessRound_ ) + ",\"isImage\":" + QString::number( old->isImage() ) +",\"data\":\"" + old->Data() + "\"}";

            emit sendMessageToPlayer( message, playerList_[ i ]);
        }

    }

    else
    {
        guessesReceived_ = 0;
        SendOutGuesses();
    }

}

void GameHostController::SendOutGuesses()
{
    QString message = "{\"type\":\"finalGuesses\",\"tRounds\":" + QString::number( guessRound_) + ",\"op\":\"" + playerList_[ guessesReceived_ ] + "\",\"word\":\"" + playerWordMap_[ playerList_[ guessesReceived_ ] ] + "\",\"data\":[";
    for ( int round = 0; round < guessRound_; round++ )
    {
        ImageGuessHandler * old = imageGuessMap_[ QPair< QString, int >( playerList_[ guessesReceived_ ], round) ];

        message.append("{\"round\":" + QString::number(round) + ",\"payload\":{\"player\":\"" + old->Player() +
                       "\",\"isImage\":" + QString::number( old->isImage() ) + ",\"data\":\"" + old->Data() +
                       "\"}},");
    }
    message = message.mid( 0, message.size() - 1 );
    message.append("]}");
    for ( int i = 0; i < playerList_.size(); i++ )
    {
        emit sendMessageToPlayer( message, playerList_[ i ] );
    }
}

void GameHostController::SendClientsGameProgress(int progress, int total)
{
    QString message = "{\"type\":\"guessProgress\",\"progress\":" + QString::number( progress ) + ",\"total\":" + QString::number( total ) + ",\"round\":" + QString::number( guessRound_ + 1 ) + ",\"roundTotal\":" + QString::number( playerList_.size() ) + "}";
    SendMessageToAll( message );
}

void GameHostController::NextFGRound()
{
    guessesReceived_++;
    if ( guessesReceived_ < guessRound_ )
    {
        SendOutGuesses();
    }
    else
    {
        GameFinished();
    }
}

void GameHostController::ReceiveGuess(int round, bool isImage, QString Guess, QString artist, QString clientID )
{
    guessMutex_.lock();
    if ( round == guessRound_ )
    {
        if ( !haveGuessesFor_.contains( clientID) )
        {
            ChatMessage( QString( clientID + " submitted!"), "Game");
            if ( isImage )
            {
                if ( !QDir(QString::number(roomCode_)).exists())
                {
                    QDir().mkdir(QString::number(roomCode_));
                }

                QFile imageFile( QString::number(roomCode_) + "/" + clientID + "_" + QString::number( fileNumber_) +".png" );
                if ( imageFile.open( QIODevice::ReadWrite) )
                {
                    imageFile.write( QByteArray::fromHex( Guess.toLatin1() ) );
                    imageFile.flush();
                    imageFile.close();

                    fileNumber_++;
                }
            }

            imageGuessMap_[ QPair< QString, int>( artist, guessRound_ ) ] = new ImageGuessHandler( this, artist, clientID, isImage, Guess );
            guessesReceived_++;
            haveGuessesFor_.append( clientID );
        }
    }

    guessMutex_.unlock();

    if ( guessesReceived_ >= playerList_.size() )
    {
        guessesReceived_ = 0;
        SendClientsGameProgress( guessesReceived_, playerList_.size() );
        haveGuessesFor_.clear();
        guessRound_++;
        ProcessImages();
    }
    else
    {
        SendClientsGameProgress( guessesReceived_, playerList_.size() );
    }
}

void GameHostController::AddPoint(QString Player)
{
    playerPointMap_[ Player ] = playerPointMap_[ Player ] + 1;
}

void GameHostController::RequestClientList(QString Player)
{
    QString message = "{\"type\":\"clientList\",\"roomCode\":" + QString::number( roomCode_ ) + ",\"clients\":[";
    foreach ( const QString player, playerList_ )
    {
        message.append("\"" + player + "\",");
    }

    message = message.mid( 0, message.size() - 1 );
    message.append( "]}");

    sendMessageToPlayer( message, Player );
}

void GameHostController::GameFinished()
{
    gameStarted_ = false;
    QMap< QString, int > playerPointMap = playerPointMap_;
    QString message = "{\"type\":\"finalScores\",\"payLoad\":[";
    foreach( const QString player, playerPointMap.keys() )
    {
        message.append( "{\"player\":\"" + player + "\",\"score\":" + QString::number( playerPointMap[ player ] ) + "}," );

    }
    message = message.mid( 0, message.size() - 1 );
    message.append( "]}");

    SendMessageToAll( message );

    ClearGuesses();
}
