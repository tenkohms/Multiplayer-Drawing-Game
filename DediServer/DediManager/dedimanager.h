#ifndef DEDIMANAGER_H
#define DEDIMANAGER_H

#include <QObject>
#include <QMap>

#include "SocketObject/gamehostsocket.h"
#include "GameHostController/gamehostcontroller.h"
#include "MessageProcessor/messageprocessor.h"

#include "LogManager/logmanager.h"

class DediManager : public QObject
{
    Q_OBJECT
public:
    DediManager(int Port = 0, QString WordFile = "");
    ~DediManager();

signals:

public slots:
    void DebugAndLog( QString message );

    void BeginConnectAttempt();
    void SocketObjectConnected();
    void SocketObjectDisconnected();
    void SocketClientDisconnected( QString clientID );
    void SendMessageToSingleClient( QString message, QString clientID );

    void CreateGame( QString clientID );
    void RemoveGame( int roomCode );
    void AddPlayerToGame( int roomCode, QString clientID );
    void ReadyToStart( int roomCode, QString sender );

    void ChatMessage( QString message, QString clientID );

    void ReceiveImageFileData( int round, QString fileData, QString artist, QString clientID);
    void ReceiveGuess( int round, QString Guess, QString artist, QString clientID);
    void ChangeTimelineRound( int roomCode, int round );
    void VoteForPlayer( int roomCode, QString Player);
    void AddPointToPlayerAndMoveToNextRound( int roomCode, QString player );
    void ReqClientList( QString player );

    void BanPlayer(QString Player);

private:
    int port_;
    QStringList wordList_;
    LogManager * logManager_;

    GameHostSocket * hostSocket;
    MessageProcessor * messageProcessor_;

    QMap< int, GameHostController * > roomCodeGameHostControllerMap_;
    QMap< QString, int > playerNameRoomCodeMap_;
    QMap< QString, int > peerIPHostCount_;
    QMap< int, QString> roomCodeIP_;
};

#endif // DEDIMANAGER_H
