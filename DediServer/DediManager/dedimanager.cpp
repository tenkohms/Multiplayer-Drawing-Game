#include "dedimanager.h"
#include <QDebug>
#include <QTimer>
#include <QTime>
#include <QFile>

#include <random>
#include <algorithm>

#include "MessageProcessor/hostmessageprocessor.h"
#include "GameHostController/gamehostcontroller.h"

DediManager::DediManager(int Port, QString WordFile )
    : QObject(nullptr),
      port_( Port )
{
    logManager_ = new LogManager( this );
    DebugAndLog( "DediManager init..." );

    roomCodeGameHostControllerMap_.clear();
    playerNameRoomCodeMap_.clear();
    peerIPHostCount_.clear();

    hostSocket = new GameHostSocket( this );
    messageProcessor_ = new HostMessageProcessor(this);

    connect( hostSocket, SIGNAL( connected()), this, SLOT(SocketObjectConnected()));
    connect( hostSocket, SIGNAL(disconnected()), this, SLOT(SocketObjectDisconnected()));
    connect( hostSocket, SIGNAL(clientDisconnected(QString)), this, SLOT( SocketClientDisconnected(QString)) );

    connect( hostSocket, SIGNAL(newMessageReceived(QString)), messageProcessor_, SLOT(ProcessMessage(QString)));
    connect( messageProcessor_, SIGNAL(registerAlias(QString,QString)), hostSocket, SLOT(RegisterAlias(QString,QString)));
    connect( messageProcessor_, SIGNAL(forwardMessage(QString)), hostSocket, SLOT(SendSocketMessage(QString)));

    connect( messageProcessor_, SIGNAL(DebugMessage(QString)), this, SLOT(DebugAndLog(QString)));
    connect( messageProcessor_, SIGNAL(CreateRoom(QString)), this, SLOT(CreateGame(QString)));
    connect( messageProcessor_, SIGNAL(JoinRoom(int,QString)), this, SLOT(AddPlayerToGame(int,QString)));
    connect( messageProcessor_, SIGNAL(chatMessage(QString,QString)), this, SLOT( ChatMessage(QString,QString)));
    connect( messageProcessor_, SIGNAL(ReadyToStart(int,QString)), this, SLOT(ReadyToStart(int,QString)));
    connect( messageProcessor_, SIGNAL(imageFileData(int,QString,QString,QString)), this, SLOT(ReceiveImageFileData(int,QString,QString,QString)));
    connect( messageProcessor_, SIGNAL(guessData(int,QString,QString,QString)), this, SLOT(ReceiveGuess(int,QString,QString,QString)));
    connect( messageProcessor_, SIGNAL( ChangeTimelineRound(int,int)), this, SLOT(ChangeTimelineRound(int,int)));
    connect( messageProcessor_, SIGNAL(VotedForPlayer(int,QString)), this, SLOT(VoteForPlayer(int,QString)));
    connect( messageProcessor_, SIGNAL(reqClientList(QString)), this, SLOT(ReqClientList(QString)));
    connect( messageProcessor_, SIGNAL(banPlayer(QString)), this, SLOT(BanPlayer(QString)));

    QFile wordFile ( WordFile );
    if ( !wordFile.open( QIODevice::ReadOnly ) )
    {
        DebugAndLog( "Unable to open file..." );
        return;
    }
    else
    {
        wordList_ = QString( wordFile.readAll() ).split(QRegExp("\n" ));
        wordList_.removeAll("");

        DebugAndLog("Wordlist: " + QString::number( wordList_.size()));
    }

    DebugAndLog("Init Done");
    BeginConnectAttempt();
}

DediManager::~DediManager()
{
    foreach( const int roomCode, roomCodeGameHostControllerMap_.keys() )
    {
        GameHostController * oldRoom = roomCodeGameHostControllerMap_[ roomCode ];
        delete oldRoom;
    }
    roomCodeGameHostControllerMap_.clear();

    delete hostSocket;
    delete messageProcessor_;
    delete logManager_;
}

void DediManager::DebugAndLog(QString message)
{
    qDebug() << message;
    logManager_->WriteLogFileData( message );
}

//SOCKETS
void DediManager::BeginConnectAttempt()
{
    DebugAndLog( "Attempting to open port: " + port_ );
    hostSocket->BeginConnection( QString::number( port_ ) );
}

void DediManager::SocketObjectConnected()
{
    DebugAndLog( "Port opened at: " + QString::number(port_) );
}

void DediManager::SocketObjectDisconnected()
{
    QTimer reconnectTimer( this );
    reconnectTimer.setSingleShot(true);
    connect( &reconnectTimer, SIGNAL(timeout()), this, SLOT(BeginConnectAttempt()));

    DebugAndLog( "Host socket closed! Waiting to repoen..." );
    reconnectTimer.start( 5000 );
}

void DediManager::SocketClientDisconnected(QString clientID)
{
    if ( playerNameRoomCodeMap_.contains( clientID ) )
    {
        roomCodeGameHostControllerMap_[ playerNameRoomCodeMap_[ clientID ] ]->RemovePlayer( clientID );
    }
    playerNameRoomCodeMap_.remove( clientID );

    DebugAndLog( "Player: " + clientID + " disconnected!" );
}

void DediManager::SendMessageToSingleClient(QString message, QString clientID)
{
    hostSocket->SendClientMessage( message, clientID );
}

//GAMEROOM


void DediManager::CreateGame(QString clientID)
{
    if ( peerIPHostCount_[ hostSocket->GetPlayerIP( clientID ) ] >= 3 )
    {
        hostSocket->KickPlayer( clientID );
        return;
    }

    DebugAndLog("DediManager::CreateGame");
    QTime time = QTime::currentTime();
    qsrand( (uint)time.msec());

    int roomCode = 0;
    do {
        roomCode = qrand() % ( 999999 );
    } while( roomCodeGameHostControllerMap_.contains( roomCode ) );

    GameHostController * newGame = new GameHostController( this, roomCode, wordList_ );

    connect( newGame, SIGNAL(emptyGame(int)), this, SLOT(RemoveGame(int)));
    connect( newGame, SIGNAL( DebugAndLog(QString)), this, SLOT( DebugAndLog(QString)) );
    connect( newGame, SIGNAL(sendMessageToPlayer(QString,QString)), this, SLOT(SendMessageToSingleClient(QString,QString)));
    roomCodeGameHostControllerMap_[ roomCode ] = newGame;
    newGame->AddPlayer( clientID );
    playerNameRoomCodeMap_[ clientID ] = roomCode;
    roomCodeIP_[ roomCode ] = hostSocket->GetPlayerIP( clientID );
    if ( peerIPHostCount_.contains(hostSocket->GetPlayerIP( clientID )) )
    {
        peerIPHostCount_[ hostSocket->GetPlayerIP( clientID ) ] = peerIPHostCount_[ hostSocket->GetPlayerIP( clientID ) ] + 1;
    }
    else
    {
        peerIPHostCount_[ hostSocket->GetPlayerIP( clientID ) ] = 1;
    }
    DebugAndLog( "Game code: " + QString::number( roomCode ) + " created.");
}

void DediManager::RemoveGame(int roomCode)
{
    GameHostController * oldGame = roomCodeGameHostControllerMap_[ roomCode ];
    delete oldGame;
    roomCodeGameHostControllerMap_.remove( roomCode );

    peerIPHostCount_[ roomCodeIP_[ roomCode ] ] = (int)(peerIPHostCount_[ roomCodeIP_[ roomCode ] ]) - 1;
    if (peerIPHostCount_[ roomCodeIP_[ roomCode ] ] <= 0 )
    {
        peerIPHostCount_.remove( roomCodeIP_[ roomCode ] );
    }

    roomCodeIP_.remove( roomCode );

    DebugAndLog("Game: " + QString::number( roomCode) + " empty. Removing game.");
}

void DediManager::AddPlayerToGame(int roomCode, QString clientID )
{
    if ( roomCodeGameHostControllerMap_.contains ( roomCode) )
    {
        if ( roomCodeGameHostControllerMap_[ roomCode ]->IsGameStarted() )
        {
            DebugAndLog( clientID + " tried to join game in progress.");
            QString message = "{\"type\":\"joinError\",\"error\":\"Game in progress\"}";
            hostSocket->SendClientMessage( message, clientID );
        }
        else
        {
            DebugAndLog( "Adding: " + clientID + " to game: " + QString::number( roomCode ));

            roomCodeGameHostControllerMap_[ roomCode ]->AddPlayer( clientID );
            playerNameRoomCodeMap_[ clientID ] = roomCode;
        }
    }
    else
    {
        DebugAndLog( clientID + " tried to join non-existent game: " + QString::number( roomCode ) );

        QString message = "{\"type\":\"joinError\",\"error\":\"Invalid room code\"}";
        hostSocket->SendClientMessage( message, clientID );
    }
}

void DediManager::ReadyToStart(int roomCode, QString sender)
{
    roomCodeGameHostControllerMap_[ roomCode ]->ReadyToStart( sender );
}

void DediManager::ChatMessage(QString message, QString clientID)
{
    roomCodeGameHostControllerMap_[ playerNameRoomCodeMap_[ clientID ] ]->ChatMessage( message, clientID );
}

void DediManager::ReceiveImageFileData( int round, QString fileData, QString artist, QString clientID)
{
    roomCodeGameHostControllerMap_[ playerNameRoomCodeMap_[ clientID] ]->ReceiveGuess(round, true, fileData, artist, clientID );
}

void DediManager::ReceiveGuess( int round, QString Guess, QString artist, QString clientID)
{
    roomCodeGameHostControllerMap_[ playerNameRoomCodeMap_[ clientID] ]->ReceiveGuess( round, false, Guess, artist, clientID );
}

void DediManager::ChangeTimelineRound(int roomCode, int round)
{
    roomCodeGameHostControllerMap_[ roomCode ]->ChangeTimelineRound( round );
}

void DediManager::VoteForPlayer(int roomCode, QString player)
{
    roomCodeGameHostControllerMap_[ roomCode ]->VotedForPlayer( player );
}

void DediManager::AddPointToPlayerAndMoveToNextRound(int roomCode, QString player)
{
    roomCodeGameHostControllerMap_[ roomCode ]->AddPoint( player );
    roomCodeGameHostControllerMap_[ roomCode ]->NextFGRound();
}

void DediManager::ReqClientList(QString player)
{
    roomCodeGameHostControllerMap_[ playerNameRoomCodeMap_[ player] ]->RequestClientList( player );
}

void DediManager::BanPlayer(QString Player)
{
    hostSocket->KickPlayer( Player );
}


