QT += core websockets
QT -= gui

CONFIG += c++11

TARGET = ScribbleDedi
CONFIG += console
CONFIG -= app_bundle

SOURCES += main.cpp \ 
    DediManager/dedimanager.cpp \
    LogManager/logmanager.cpp \
    SocketObject/gamehostsocket.cpp \
    SocketObject/gamehostclientsocket.cpp \
    MessageProcessor/hostmessageprocessor.cpp \
    GameHostController/gamehostcontroller.cpp \
    ImageGuessHandler/imageguesshandler.cpp \
    SocketObject/socketobjectbase.cpp \
    MessageProcessor/messageprocessor.cpp
DEFINES += QT_DEPRECATED_WARNINGS

HEADERS += \
    DediManager/dedimanager.h \
    LogManager/logmanager.h \
    SocketObject/gamehostsocket.h \
    SocketObject/gamehostclientsocket.h \ 
    MessageProcessor/hostmessageprocessor.h \
    GameHostController/gamehostcontroller.h \
    ImageGuessHandler/imageguesshandler.h \
    SocketObject/socketobjectbase.h \
    MessageProcessor/messageprocessor.h

#INCLUDEPATH += ../Common
#unix:LIBS += -L../Common -lCommon
#win32:LIBS += -LC:/Millipore/build-DrawGame-Desktop_Qt_5_12_5_MSVC2017_64bit-Release/Common/release -lCommon

DISTFILES += \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/res/values/libs.xml

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

ANDROID_ABIS = armeabi-v7a
