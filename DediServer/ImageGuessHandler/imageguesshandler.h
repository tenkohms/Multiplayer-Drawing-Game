#ifndef IMAGEGUESSHANDLER_H
#define IMAGEGUESSHANDLER_H

#include <QObject>
#include <QMap>

class ImageGuessHandler : public QObject
{
    Q_OBJECT
public:
    explicit ImageGuessHandler(QObject *parent = nullptr, QString Artist = "", QString Player ="", bool isImage = false, QString Data ="");
    QString Artist();
    QString Player();
    bool isImage();
    QString Data();
signals:

public slots:

private:
    bool isImage_;
    QString artist_;
    QString player_;
    QString data_;
};

#endif // IMAGEGUESSHANDLER_H
