#include "imageguesshandler.h"

ImageGuessHandler::ImageGuessHandler(QObject *parent, QString Artist, QString Player, bool isImage, QString Data) :
    QObject(parent),
    isImage_( isImage ),
    artist_( Artist ),
    player_( Player),
    data_( Data )
{

}

QString ImageGuessHandler::Artist()
{
    return artist_;
}

QString ImageGuessHandler::Player()
{
    return player_;
}

bool ImageGuessHandler::isImage()
{
    return isImage_;
}

QString ImageGuessHandler::Data()
{
    return data_;
}
