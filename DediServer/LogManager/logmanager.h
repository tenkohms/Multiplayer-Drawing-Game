#ifndef LOGMANAGER_H
#define LOGMANAGER_H

#include <QObject>
#include <QMutex>

class LogManager : public QObject
{
    Q_OBJECT
public:
    explicit LogManager(QObject *parent = nullptr);
    void WriteLogFileData( QString data );
signals:

public slots:

private:
    QString fileName_;
    QMutex fileWriteMutex_;
};

#endif // LOGMANAGER_H
