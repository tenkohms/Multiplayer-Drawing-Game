#include "logmanager.h"
#include <QFile>

LogManager::LogManager(QObject *parent)
    : QObject(parent)
{
    fileName_ = "test.log";
    QFile logFile( fileName_ );
    if ( logFile.open( QIODevice::ReadWrite | QIODevice::Append | QIODevice::Text ) )
    {
        logFile.flush();
        logFile.close();
    }
}

void LogManager::WriteLogFileData(QString data)
{
    fileWriteMutex_.lock();
    QFile logFile( fileName_ );
    if ( logFile.open( QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text ) )
    {
        logFile.write(QString ( data + "\n").toLatin1());
        logFile.flush();
        logFile.close();
    }
    fileWriteMutex_.unlock();
}

