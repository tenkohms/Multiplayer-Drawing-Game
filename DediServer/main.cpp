#include <QCoreApplication>

#include "DediManager/dedimanager.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    DediManager mDedi( 443, "wordList.txt");

    return a.exec();
}
