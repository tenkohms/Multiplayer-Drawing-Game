#ifndef HOSTMESSAGEPROCESSOR_H
#define HOSTMESSAGEPROCESSOR_H

#include "MessageProcessor/messageprocessor.h"
#include <QMutex>
class GameHostSocket;

class HostMessageProcessor : public MessageProcessor
{
    Q_OBJECT
public:
    HostMessageProcessor(QObject *parent = nullptr);
    void ProcessMessage( QString message);
    void checkQueue();

signals:

private:
    QStringList m_messageQueue;
    QMutex m_locker;
};

#endif // HOSTMESSAGEPROCESSOR_H
