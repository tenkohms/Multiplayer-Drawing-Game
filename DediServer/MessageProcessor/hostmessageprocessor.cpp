#include "hostmessageprocessor.h"
#include <QFile>
#include <QCryptographicHash>
#include <QMessageAuthenticationCode>
#include <QDebug>

HostMessageProcessor::HostMessageProcessor(QObject *parent) :
    MessageProcessor(parent)
{
    connect( this, &HostMessageProcessor::readyNext, this, &HostMessageProcessor::checkQueue);
}

void HostMessageProcessor::checkQueue() {
    if ( m_messageQueue.isEmpty() )
        return;

    m_locker.lock();
    QString message = m_messageQueue.first();
    m_messageQueue.pop_front();

    if ( testIgnore_ )
    {
        return;
    }
    if ( GetMessageType( message ) == "registerAlias" )
    {
        //A client wants a new name
        QString oldAlias = QJsonDocument::fromJson( message.toLatin1() ).object().value("oldAlias").toString();
        QString newAlias = QJsonDocument::fromJson( message.toLatin1() ).object().value("newAlias").toString();
        emit registerAlias( oldAlias, newAlias );
    }

    if ( GetMessageType( message ) == "chatMessage" )
    {
        //received a new chat message
        QString payload = QJsonDocument::fromJson( message.toLatin1() ).object().value( "message" ).toString();
        QString clientID = QJsonDocument::fromJson( message.toLatin1() ).object().value( "sender" ).toString();
        QString hash = QJsonDocument::fromJson( message.toLatin1() ).object().value( "checksum" ).toString();
        QByteArray key = "031190";
        QMessageAuthenticationCode code( QCryptographicHash::Sha1 );
        code.setKey( key );
        code.addData( payload.toLatin1() );
        QString gen = code.result().toHex();

        if( hash != gen )
        {
            emit banPlayer( clientID );
        }
        else
        {
            //lets receive this message to the UI as well as forward it to the clientlist
            emit chatMessage( payload, clientID );
        }
    }

    if ( GetMessageType( message ) == "fileTransfer" )
    {
        QString md5 = QJsonDocument::fromJson( message.toLatin1() ).object().value( "checksum" ).toString();
        QString fileData = QJsonDocument::fromJson( message.toLatin1() ).object().value( "data" ).toString();
        QString sender = QJsonDocument::fromJson( message.toLatin1() ).object().value( "sender" ).toString();
        QString artist = QJsonDocument::fromJson( message.toLatin1() ).object().value( "artist" ).toString();
        int round = QJsonDocument::fromJson( message.toLatin1() ).object().value( "round" ).toInt();

        QByteArray key = "031190";
        QMessageAuthenticationCode code( QCryptographicHash::Sha1 );
        code.setKey( key );
        code.addData( fileData.toLatin1() );
        QString gen = code.result().toHex();

        if( md5 != gen )
        {
            emit banPlayer( sender );
        }
        else
        {
            emit imageFileData( round, fileData, artist, sender );
        }
    }

    if ( GetMessageType( message ) == "guessWord" )
    {
        QString guess = QJsonDocument::fromJson( message.toLatin1() ).object().value( "guess" ).toString();
        QString sender = QJsonDocument::fromJson( message.toLatin1() ).object().value( "sender" ).toString();
        QString artist = QJsonDocument::fromJson( message.toLatin1() ).object().value( "artist" ).toString();
        int round = QJsonDocument::fromJson( message.toLatin1() ).object().value( "round" ).toInt();
        emit guessData( round, guess, artist, sender );
    }

    if ( GetMessageType( message ) == "changeRound" )
    {
        int roomCode = QJsonDocument::fromJson( message.toLatin1() ).object().value("roomCode").toInt();
        int round = QJsonDocument::fromJson( message.toLatin1() ).object().value( "round" ).toInt();

        emit ChangeTimelineRound( roomCode, round );
    }

    if ( GetMessageType( message ) == "voteSelect" )
    {
        int roomCode = QJsonDocument::fromJson( message.toLatin1() ).object().value("roomCode").toInt();
        QString player = QJsonDocument::fromJson( message.toLatin1() ).object().value("player").toString();

        emit VotedForPlayer( roomCode, player );
    }

    if ( GetMessageType( message ) == "finalScores" )
    {
        emit forwardMessage( message );
    }

    if ( GetMessageType( message ) == "reqClientList" )
    {
        QString clientID = QJsonDocument::fromJson( message.toLatin1() ).object().value( "sender" ).toString();
        emit reqClientList( clientID );
    }

    if ( GetMessageType( message) == "createRoom" )
    {
        QString clientID = QJsonDocument::fromJson( message.toLatin1() ).object().value("clientID").toString();
        emit CreateRoom( clientID );
    }

    if ( GetMessageType( message) == "joinRoom" )
    {
        QString clientID = QJsonDocument::fromJson( message.toLatin1() ).object().value("clientID").toString();
        int roomCode = QJsonDocument::fromJson( message.toLatin1() ).object().value("roomCode").toInt();
        emit JoinRoom( roomCode, clientID );
    }

    if ( GetMessageType( message ) == "readyToStart" )
    {
        int roomCode = QJsonDocument::fromJson( message.toLatin1() ).object().value("roomCode").toInt();
        QString sender = QJsonDocument::fromJson( message.toLatin1() ).object().value( "sender" ).toString();
        emit ReadyToStart( roomCode, sender );
    }


    MessageProcessor::ProcessMessage( message );
    m_locker.unlock();

    emit readyNext();
}

void HostMessageProcessor::ProcessMessage(QString message)
{
    m_messageQueue.append(message);
    emit readyNext();
}
