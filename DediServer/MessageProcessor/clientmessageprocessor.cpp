#include "clientmessageprocessor.h"

ClientMessageProcessor::ClientMessageProcessor(QObject *parent) :
    MessageProcessor(parent)
{

}

void ClientMessageProcessor::ProcessMessage(QString message)
{

    if ( GetMessageType( message ) == "registerAliasSuccess" )
    {
        //received fail success message from host
        //get the new alias and send signal to update gameclientsocket identity
        QString newAlias( QJsonDocument::fromJson( message.toLatin1() ).object().value( "alias" ).toString() );
        emit registerAlias( "dummy", newAlias );

        //used to let setaliaswrapper know we succeeded, move on
        emit aliasSetFailed( false );
    }

    if ( GetMessageType( message ) == "registerAliasFail" )
    {
        //received fail alias message from host
        emit aliasSetFailed( true );
    }

    if ( GetMessageType( message ) == "clientList" )
    {
        //received list of clients, create qstringlist from jsonarray and emit signal
        QStringList payload;
        QJsonArray clientArray = QJsonDocument::fromJson( message.toLatin1() ).object().value( "clients" ).toArray();
        foreach ( QJsonValue val, clientArray)
            payload.append( val.toString() );

        int roomCode = QJsonDocument::fromJson( message.toLatin1() ).object().value( "roomCode" ).toInt();

        emit clientList( roomCode, payload );
    }

    if ( GetMessageType( message ) == "chatMessage" )
    {
        QString payload = QJsonDocument::fromJson( message.toLatin1() ).object().value( "message" ).toString();
        QString clientID = QJsonDocument::fromJson( message.toLatin1() ).object().value( "sender" ).toString();
        emit chatMessage( payload, clientID );
    }
    if ( GetMessageType( message ) == "guessProgress" )
    {
        int progress = QJsonDocument::fromJson( message.toLatin1() ).object().value( "progress" ).toInt();
        int total = QJsonDocument::fromJson( message.toLatin1() ).object().value( "total" ).toInt();
        int round = QJsonDocument::fromJson( message.toLatin1() ).object().value( "round" ).toInt();
        int roundTotal = QJsonDocument::fromJson( message.toLatin1() ).object().value( "roundTotal" ).toInt();
        emit guessesReceivedProgress( progress, total, round, roundTotal);
    }

    if ( GetMessageType( message ) == "roundReady" )
    {
        QString word = QJsonDocument::fromJson( message.toLatin1() ).object().value("word").toString();
        emit roundReady( word );
    }

    if ( GetMessageType( message ) == "newGuessRound" )
    {
        int round = QJsonDocument::fromJson( message.toLatin1() ).object().value("round").toInt();
        QString data = QJsonDocument::fromJson( message.toLatin1() ).object().value("data").toString();
        bool isImage = QJsonDocument::fromJson( message.toLatin1() ).object().value("isImage").toInt();
        QString artist = QJsonDocument::fromJson( message.toLatin1() ).object().value("op").toString();
        emit newGuessRound( round, artist, isImage, data );
    }

    if ( GetMessageType( message ) == "finalGuesses" )
    {
        QMap< int, QPair< QString, QString > > roundPlayerGuess;
        QStringList displayWord;
        QMap< int, bool > roundIsImage;
        QString origWord = QJsonDocument::fromJson( message.toLatin1() ).object().value("word").toString();
        QJsonArray roundArray = QJsonDocument::fromJson( message.toLatin1() ).object().value("data").toArray();
        QString op = QJsonDocument::fromJson( message.toLatin1() ).object().value("op").toString();
        int rounds = QJsonDocument::fromJson( message.toLatin1() ).object().value("tRounds").toInt() + 1;
        roundPlayerGuess[ 0 ] = QPair< QString, QString >( "origWord", origWord );
        displayWord.append(roundArray[ 0 ].toObject().value( "payload" ).toObject().value("player").toString() + "'s word was");
        roundIsImage[ 0 ] = false;
        for ( int round = 0; round < roundArray.size(); round++ )
        {
            roundPlayerGuess[ roundArray.at( round ).toObject().value("round").toInt() + 1] = QPair< QString, QString>( roundArray[ round ].toObject().value( "payload" ).toObject().value("player").toString(), roundArray[ round ].toObject().value( "payload" ).toObject().value("data").toString());

            if ( roundArray[ round ].toObject().value( "payload" ).toObject().value("isImage").toInt() == 1)
            {
                roundIsImage[ roundArray.at( round ).toObject().value("round").toInt() + 1 ] = true;
                displayWord.append(roundArray[ round ].toObject().value( "payload" ).toObject().value("player").toString() + " drew");
            }
            else
            {
                roundIsImage[ roundArray.at( round ).toObject().value("round").toInt() + 1 ] = false;
                displayWord.append(roundArray[ round ].toObject().value( "payload" ).toObject().value("player").toString().replace("\"","\\\"") + " guessed");
            }

        }

        emit finalGuesses( rounds, op, origWord, roundIsImage, roundPlayerGuess, displayWord );
    }

    if ( GetMessageType( message ) == "changeRound" )
    {
        int round = QJsonDocument::fromJson( message.toLatin1() ).object().value("round").toInt();
        emit changeRound( round );
    }

    if ( GetMessageType( message ) == "voteSelect" )
    {
        QString player = QJsonDocument::fromJson( message.toLatin1() ).object().value("player").toString();
        emit voteSelect( player );
    }

    if ( GetMessageType( message ) == "finalScores" )
    {
        QMap< QString, int > scores;

        QJsonArray jsonArray = QJsonDocument::fromJson( message.toLatin1() ).object().value( "payLoad" ).toArray();
        for( int i = 0; i < jsonArray.size(); i++ )
        {
            scores[ jsonArray.at( i ).toObject().value("player").toString() ] = jsonArray.at( i ).toObject().value("score").toInt();
        }
        emit finalScores( scores );

    }

    if ( GetMessageType( message ) == "joinError" )
    {
        QString errorMessage = QJsonDocument::fromJson( message.toLatin1() ).object().value("error").toString();
        emit joinError( errorMessage );
    }

    if ( GetMessageType( message ) == "ipBan" )
    {
        emit ipBan();
    }

    if ( GetMessageType( message ) == "readyList" )
    {
        QStringList readyList;

        QJsonArray jsonArray = QJsonDocument::fromJson( message.toLatin1() ).object().value( "ready" ).toArray();
        foreach( const QJsonValue val, jsonArray )
            readyList.append( val.toString() );
        emit ReadyList(readyList);
    }

   MessageProcessor::ProcessMessage( message );
}
