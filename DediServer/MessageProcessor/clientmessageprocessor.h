#ifndef CLIENTMESSAGEPROCESSOR_H
#define CLIENTMESSAGEPROCESSOR_H

#include "MessageProcessor/messageprocessor.h"

class ClientMessageProcessor : public MessageProcessor
{

public:
    ClientMessageProcessor(QObject *parent = nullptr);
    void ProcessMessage( QString message );

signals:
};

#endif // CLIENTMESSAGEPROCESSOR_H
