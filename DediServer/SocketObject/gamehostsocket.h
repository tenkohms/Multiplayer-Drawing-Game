#ifndef GAMEHOSTSOCKET_H
#define GAMEHOSTSOCKET_H

#include <QObject>
#include <QMap>
#include "gamehostclientsocket.h"
#include "SocketObject/socketobjectbase.h"

QT_FORWARD_DECLARE_CLASS(QWebSocketServer)
QT_FORWARD_DECLARE_CLASS(QWebSocket)

class GameHostSocket : public SocketObjectBase
{
    Q_OBJECT
public:
    explicit GameHostSocket(QObject *parent = nullptr);
    ~GameHostSocket();
    void BeginConnection( QString );
    void SendSocketMessage( QString );
    void RegisterAlias( QString Alias );
    void DoNotAllowNewConnections();
    void AllowConnections();
    void ResendMessage();

    QString GetPlayerIP( QString clientID);
    void KickPlayer( QString clientID );

signals:

public slots:
    void SendClientMessage(QString, QString);
    void RegisterAlias( QString OldAlias, QString NewAlias );

private Q_SLOTS:
    void onNewConnection();
    void socketDisconnected(QString clientID);

private:
    bool allowConnections_;
    QStringList banList_;
    QWebSocketServer *webSocketServer_;
    QMap <QString, GameHostClientSocket * >clientMap_;
};

#endif // GAMEHOSTSOCKET_H
