#ifndef GAMEHOSTCLIENTSOCKET_H
#define GAMEHOSTCLIENTSOCKET_H

#include <QObject>
#include <QWebSocket>

class GameHostClientSocket : public QObject
{
    Q_OBJECT
public:
    explicit GameHostClientSocket(QObject *parent, QWebSocket * TheClient, QString Identifier);
    ~GameHostClientSocket();
    QString whoAmI();
    void SendTextMessageToClient( QString message );
    void SetNewAlias( QString Alias );
    void CloseConnection();
    void ResendMessage();
    QString LastMessage();

    QString GetIP();

signals:
    void newMessage( QString );
    void removeMe(QString);
public slots:

private Q_SLOTS:
    void newTextMessageReceived( QString message );
    void onDisconnect();

private:
    QWebSocket * theClient_;
    QString identifier_;
    QString queuedMessage_;
};

#endif // GAMEHOSTCLIENTSOCKET_H

