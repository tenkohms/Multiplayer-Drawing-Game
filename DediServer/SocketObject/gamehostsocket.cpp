#include "gamehostsocket.h"
#include "QtWebSockets/QWebSocketServer"
#include "QtWebSockets/QWebSocket"

GameHostSocket::GameHostSocket(QObject *parent) :
    SocketObjectBase( parent ),
    allowConnections_( true ),
    webSocketServer_(Q_NULLPTR)
{
    webSocketServer_ = new QWebSocketServer(QStringLiteral("Scribble Server"),
                                              QWebSocketServer::NonSecureMode,
                                              this);
    connect(webSocketServer_, &QWebSocketServer::newConnection,
            this, &GameHostSocket::onNewConnection);
    connect( webSocketServer_, SIGNAL(closed()), this, SLOT(connectionClosing()));

    identity_ = "gameserver";
    banList_.clear();
}

GameHostSocket::~GameHostSocket()
{
    //cleanup
    foreach( GameHostClientSocket * oldClient, clientMap_ )
    {
        oldClient->CloseConnection();
        delete oldClient;
    }

    delete webSocketServer_;
}

void GameHostSocket::AllowConnections()
{
    allowConnections_ = true;
}

void GameHostSocket::DoNotAllowNewConnections()
{
    allowConnections_ = false;
}

void GameHostSocket::BeginConnection(QString Port)
{
    //try to listen on port
    if (webSocketServer_->listen(QHostAddress::Any, Port.toInt()))
    {
        //wow that worked
        emit connected();
    }
}

void GameHostSocket::onNewConnection()
{
    if ( allowConnections_ )
    {
        //new socket connection made
        //create container for new socket
        QWebSocket * pSocket = webSocketServer_->nextPendingConnection();

        if ( banList_.contains( pSocket->peerAddress().toString() ) )
        {
            pSocket->sendTextMessage("{\"type\":\"ipBan\"}");
            pSocket->close();
            delete pSocket;
            return;
        }
        //create generic client name cl + size of clientlist
        int counter = 0;
        QString clientName = QString( "cl" + QString::number( counter ));
        while ( clientMap_.contains( clientName) )
        {
            counter++;
            clientName = QString( "cl" + QString::number( counter ));
        }

        //create new instance of GameHostClientSocekt, and give it the socket and generic name
        GameHostClientSocket * newSocketClient = new GameHostClientSocket( this, pSocket, clientName );

        // GameHostClientSocket <--> GameHostSocket signal slot connections
        connect( newSocketClient, SIGNAL(newMessage(QString)), this, SLOT(newSocketMessage(QString)));
        connect ( newSocketClient, SIGNAL(removeMe(QString)), this, SLOT( socketDisconnected(QString)));

        //add new client to our map with key being its generic name
        clientMap_.insert( newSocketClient->whoAmI(), newSocketClient );

        //create json message to let our new client what we decided to call him/her
        QString initMessage = "{\"type\":\"registerAliasSuccess\",\"alias\":\"" + clientName + "\"}";

        //let 'em know
        SendClientMessage( initMessage, clientName );
    }
    else
    {
        QWebSocket * pSocket = webSocketServer_->nextPendingConnection();
        pSocket->abort();
        delete pSocket;
    }
}

void GameHostSocket::socketDisconnected(QString clientID)
{
    //a client disconnected. Let's remove it from our map and clean it up
    GameHostClientSocket * oldClient;
    oldClient = *( clientMap_.find( clientID ));
    oldClient->deleteLater();
    clientMap_.remove( clientID );

    emit clientDisconnected( clientID );
}

void GameHostSocket::SendSocketMessage(QString message)
{
    //function to send a message to ALL our clients
    foreach (GameHostClientSocket * client, clientMap_)
    {
        client->SendTextMessageToClient( message );
    }
}

void GameHostSocket::RegisterAlias(QString Alias)
{

}

void GameHostSocket::ResendMessage()
{
    //function to send a message to ALL our clients
    foreach (GameHostClientSocket * client, clientMap_)
    {
        client->ResendMessage();
    }
}

QString GameHostSocket::GetPlayerIP(QString clientID)
{
    GameHostClientSocket * tempClient = clientMap_[ clientID ];
    return tempClient->GetIP();
}

void GameHostSocket::KickPlayer(QString clientID)
{
    banList_.append( clientMap_[ clientID ]->GetIP() );
    clientMap_[ clientID ]->CloseConnection();
}

void GameHostSocket::SendClientMessage(QString message, QString clientID)
{
    //function to send message to a single client
    clientMap_[ clientID ]->SendTextMessageToClient( message );

}

void GameHostSocket::RegisterAlias(QString OldAlias, QString NewAlias)
{
    QString message;

    bool goodName = true;
    for ( int i = 0 ; i < NewAlias.size(); i++ )
    {
        if ( !NewAlias[ i ].isLetter() && !NewAlias[ i ].isDigit() )
        {
            goodName = false;
        }
    }
    if ( goodName)
    {
        int userCounter = 1;
        QString alias = NewAlias;
        while( clientMap_.contains(alias) )
        {
            alias = NewAlias;
            userCounter++;
            alias.append( QString::number( userCounter ) );
        }

        //set register alias success message
        message = "{\"type\":\"registerAliasSuccess\",\"alias\":\"" + alias + "\"}";

        //rename socket in our map
        GameHostClientSocket * tClient = clientMap_[ OldAlias ];
        clientMap_.remove( OldAlias );
        clientMap_[ alias ] = tClient;
        tClient->SetNewAlias( alias );

        //send success message to our new socket
        SendClientMessage( message, alias);
    }
    else
    {
        message = "{\"type\":\"registerAliasFail\"}";
        SendClientMessage( message, OldAlias );
    }

//    if ( !waitingForPlayer_ )
//    {

//    }
//    else
//    {
//        if ( NewAlias != disconnectedPlayer_)
//        {
//            GameHostClientSocket * oldClient = tempClientSocket;
//            oldClient->CloseConnection();
//            delete oldClient;
//            tempClientSocket = Q_NULLPTR;
//        }
//        else
//        {
//            GameHostClientSocket * reAddPlayer = tempClientSocket;
//            reAddPlayer->SetNewAlias( disconnectedPlayer_);
//            clientMap_[ disconnectedPlayer_ ] = reAddPlayer;
//            clientMap_[ disconnectedPlayer_ ]->SendTextMessageToClient( lastMessage_ );
//            tempClientSocket = Q_NULLPTR;
//            disconnectedPlayer_ = QString();
//            lastMessage_ = QString();
//            allowConnections_ = false;
//            waitingForPlayer_ = false;
//            emit closeClientDisconnectWindow();
//        }
//    }
}
