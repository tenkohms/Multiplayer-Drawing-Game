#include "gamehostclientsocket.h"

GameHostClientSocket::GameHostClientSocket(QObject *parent, QWebSocket * TheClient, QString Identifier) :
    QObject(parent),
    theClient_( TheClient ),
    identifier_( Identifier ),
    queuedMessage_( QString() )

{
    connect( theClient_, SIGNAL(textMessageReceived(QString)), this, SLOT( newTextMessageReceived(QString)));
    connect( theClient_, SIGNAL(disconnected()), this, SLOT(onDisconnect()));
}

GameHostClientSocket::~GameHostClientSocket()
{
    theClient_->deleteLater();
}


QString GameHostClientSocket::whoAmI()
{
    return identifier_;
}

void GameHostClientSocket::newTextMessageReceived(QString message)
{
    emit newMessage(message);
}

void GameHostClientSocket::onDisconnect()
{
    emit removeMe(identifier_);
}

void GameHostClientSocket::CloseConnection()
{
    theClient_->abort();
}

void GameHostClientSocket::ResendMessage()
{
    SendTextMessageToClient( queuedMessage_ );
}

QString GameHostClientSocket::LastMessage()
{
    return queuedMessage_;
}

QString GameHostClientSocket::GetIP()
{
    QString retVal( theClient_->peerAddress().toString() );
    return retVal;
}

void GameHostClientSocket::SendTextMessageToClient(QString message)
{
    if ( message.contains( "roundReady") || message.contains( "newGuessRound") )
        queuedMessage_ = message;
    theClient_->sendTextMessage( message );
}

void GameHostClientSocket::SetNewAlias(QString Alias)
{
    identifier_ = Alias;
}
