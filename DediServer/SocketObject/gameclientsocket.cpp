#include "gameclientsocket.h"
#include <QUrl>
#include <QDebug>

GameClientSocket::GameClientSocket(QObject *parent) :
    SocketObjectBase( parent ),
    clientWebSocket_( Q_NULLPTR ),
    queuedMessage_( QString() )
{
    clientWebSocket_ = new QWebSocket;
    connect( clientWebSocket_, SIGNAL(connected()), this, SLOT(connectionOpen()));
    connect( clientWebSocket_, SIGNAL(disconnected()), this, SLOT(connectionClosing()));
    connect( clientWebSocket_, SIGNAL(textMessageReceived(QString)), this, SLOT(newSocketMessage(QString)));
}

GameClientSocket::~GameClientSocket()
{
    clientWebSocket_->close();
    delete clientWebSocket_;
}

void GameClientSocket::SendSocketMessage(QString message)
{
    if ( message.contains( "fileTransfer" ) || message.contains( "guessWord" ) )
    {
        queuedMessage_ = message;
    }

    if( isConnected_ )
        clientWebSocket_->sendTextMessage( message );
}

void GameClientSocket::RegisterAlias(QString Alias)
{
    QString message = "{\"type\":\"registerAlias\",\"newAlias\":\"" + Alias + "\",\"oldAlias\":\"" + identity_ + "\"}";
    SendSocketMessage( message );
}

void GameClientSocket::BeginConnection( QString host)
{
    QString address( "ws://" + host );
    clientWebSocket_->open( QUrl( address ) );
}

void GameClientSocket::RegisterAlias(QString OldAlias, QString NewAlias)
{
    OldAlias.append("2");
    if ( identity_.isEmpty() || identity_ == "" || identity_ == " ")
    {
        identity_ = NewAlias;
        emit readyForUsername();
    }
    else
    {
        identity_ = NewAlias;
        emit readyToHostOrJoin();
    }
}

void GameClientSocket::ResendMessage()
{
    SendSocketMessage( queuedMessage_ );
}
