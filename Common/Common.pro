TEMPLATE = lib
target.path = ../GUIClient/$$TARGET
INSTALLS += target
QT += websockets
CONFIG += c++11
SOURCES += \
    SocketObject/socketobjectbase.cpp \
    MessageProcessor/messageprocessor.cpp 

HEADERS += \
    SocketObject/socketobjectbase.h \
    MessageProcessor/messageprocessor.h 
