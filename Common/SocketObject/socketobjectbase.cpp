#include "socketobjectbase.h"

#include <QCryptographicHash>
#include <QMessageAuthenticationCode>

SocketObjectBase::SocketObjectBase(QObject *parent) :
    QObject(parent),
    isConnected_( false ),
    identity_(QString())
{

}

void SocketObjectBase::newSocketMessage(QString message)
{
    emit newMessageReceived( message );
}

void SocketObjectBase::connectionOpen()
{
    isConnected_ = true;
    emit connected();
}

void SocketObjectBase::connectionClosing()
{
    isConnected_ = false;
    emit disconnected();
}

void SocketObjectBase::BroadcastChatMessage(QString message)
{
    QByteArray key = "031190";
    QMessageAuthenticationCode code( QCryptographicHash::Sha1 );
    code.setKey( key );
    code.addData( message.toLatin1() );
    QString hash = code.result().toHex();
    QString jsonMessage( "{\"type\":\"chatMessage\",\"checksum\":\"" + hash + "\",\"message\":\"" + message +
                         "\",\"sender\":\"" + identity_ + "\"}");
    SendSocketMessage(jsonMessage);
}

bool SocketObjectBase::isConnected() {
    return isConnected_;
}

QString SocketObjectBase::WhoAmI()
{
    return identity_;
}

void SocketObjectBase::SendClientMessage(QString, QString)
{

}

QString SocketObjectBase::GetPlayerIP( QString clientID)
{
    return QString();
}

void SocketObjectBase::KickPlayer(QString ClientID)
{

}

void SocketObjectBase::SendClientList()
{
}
