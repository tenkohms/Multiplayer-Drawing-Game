#ifndef SOCKETOBJECTBASE_H
#define SOCKETOBJECTBASE_H

#include <QObject>

class SocketObjectBase : public QObject
{
    Q_OBJECT
public:
    explicit SocketObjectBase(QObject *parent = nullptr);
    bool isConnected();
    virtual void BeginConnection( QString ) = 0;
    virtual void RegisterAlias( QString ) = 0;
    virtual QString WhoAmI();
    virtual void ResendMessage() = 0;

    virtual QString GetPlayerIP( QString clientID);
    virtual void KickPlayer( QString ClientID );
signals:
    void newMessageReceived( QString message );
    void connected();
    void disconnected();
    void clientDisconnected( QString );

    //client
    void readyForUsername();
    void readyToHostOrJoin();

public slots:
    virtual void SendSocketMessage( QString  ) = 0;
    virtual void newSocketMessage( QString message );
    virtual void SendClientMessage( QString, QString );
    virtual void connectionClosing();
    virtual void connectionOpen();
    virtual void RegisterAlias(QString OldAlias, QString NewAlias) = 0;
    virtual void SendClientList();
    virtual void BroadcastChatMessage( QString message );

protected:
    bool isConnected_;
    QString identity_;
};

#endif // SOCKETOBJECTBASE_H
